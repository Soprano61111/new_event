package com.digitalhorizon.eve.ui.fragment.manager

import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.view.ViewPager
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.AccountManager
import com.digitalhorizon.eve.common.FragmentWrapper
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardState
import com.digitalhorizon.eve.mvp.presenter.manager.ManagerPresenter
import com.digitalhorizon.eve.mvp.view.manager.ManagerView
import com.digitalhorizon.eve.ui.adapter.FragmentPagerAdapter
import com.digitalhorizon.eve.ui.fragment.FragmentLifeCycle
import com.digitalhorizon.eve.ui.widget.SearchView
import com.digitalhorizon.eve.utils.hideKeyboard
import com.digitalhorizon.eve.utils.showKeyboard
import com.paginate.Paginate
import com.pawegio.kandroid.d
import com.pawegio.kandroid.visible
import kotlinx.android.synthetic.main.fragment_base_guest_manager.*
import kotlinx.android.synthetic.main.view_toolbar_with_search_filter.*

abstract class BaseManagerFragment : FragmentWrapper(),
        ManagerView,
        SearchView.OnOpeningStateListener,
        ViewPager.OnPageChangeListener,
        Paginate.Callbacks {

    @InjectPresenter
    lateinit var managerPresenter: ManagerPresenter
    private var fragmentPagerAdapter: FragmentPagerAdapter? = null
    private var currentPageNumber = 0
    private var loading = false

    override fun getFragmentLayout() = R.layout.fragment_base_guest_manager

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolbarBackgroundDrawable(R.drawable.bg_vertical_gradient)
        setTitleColor(R.color.white)
        showToolbar(TOOLBAR_EMBEDDED, R.layout.view_toolbar_with_search_filter)
        setHomeAsUpIndicator(TOOLBAR_INDICATOR_HAMBURGER, R.color.white)

        if (fragmentPagerAdapter == null) {
            fragmentPagerAdapter = FragmentPagerAdapter(childFragmentManager)
            fragmentPagerAdapter?.fragmentList?.addAll(getFragmentList())
        }

        initFoundList()
        viewPager.adapter = fragmentPagerAdapter
        viewPager.setPagingEnabled(false)
        fragmentPagerAdapter?.notifyDataSetChanged()
        tabLayout.setupWithViewPager(viewPager)
        prepareTabLayout()

        search.setOnOpeningStateListener(this)
        ivSearch.setOnClickListener {
            search.openSearch()
        }

        filter.setOnClickListener {
            if (fvFilter.isFilterViewOpen) {
                fvFilter.closeFilterView()
            } else {
                fvFilter.openFilterView()
            }
        }

        fvFilter.addFilterBlock(getString(R.string.priority), arrayListOf(
                getString(GuestCardState.SENDFORAPPROVAL.strResId),
                getString(GuestCardState.SAVED.strResId),
                getString(GuestCardState.AGREED.strResId),
                getString(GuestCardState.REFUSED.strResId)
        ), arrayListOf(
                GuestCardState.SENDFORAPPROVAL,
                GuestCardState.SAVED,
                GuestCardState.AGREED,
                GuestCardState.REFUSED
        ))

        AccountManager.getPriorities()
                .subscribe({ priorities ->
                    val priorityNames: ArrayList<String> = arrayListOf()
                    val priorityIds: ArrayList<Int> = arrayListOf()
                    priorities.forEach {
                        priorityNames.add(it.name)
                        priorityIds.add(it.id)
                    }
                    fvFilter.addFilterBlock(getString(R.string.priority), priorityNames, priorityIds)
                }, {
                    d("Get priorities error")
                })
    }

    override fun onPause() {
        super.onPause()
        getFragmentLifeCycle(currentPageNumber)?.fragmentOnPause()
        search.closeSearch()
        fvFilter.closeFilterView()
    }

    override fun onResume() {
        super.onResume()
        getFragmentLifeCycle(currentPageNumber)?.fragmentOnResume()
    }

    private fun initFoundList() {
//        rvFoundList.adapter = adapter
//        rvFoundList.layoutManager = LinearLayoutManager(activity)
//        Paginate.with(rvFoundList, this).build()
    }

    private fun getFragmentLifeCycle(position: Int): FragmentLifeCycle? {
        return fragmentPagerAdapter?.fragmentList?.get(position) as? FragmentLifeCycle
    }

    override fun onPageScrollStateChanged(state: Int) {

    }

    override fun onPageScrolled(position: Int, positionOffset: Float, positionOffsetPixels: Int) {

    }

    override fun onPageSelected(position: Int) {
        if (currentPageNumber != position) {
            getFragmentLifeCycle(currentPageNumber)?.fragmentOnPause()
        } else {
            getFragmentLifeCycle(position)?.fragmentOnResume()
        }
    }

    override fun onLoadMore() {
        loading = true
        managerPresenter.getGuestCards()
    }

    override fun isLoading() = loading

    override fun hasLoadedAllItems() = managerPresenter.isLoadedAllItems()

    protected abstract fun getFragmentList(): ArrayList<Fragment>

    protected abstract fun prepareTabLayout()

    override fun onSearchClose() {
        activity?.hideKeyboard()
        setToolbarBackgroundDrawable(R.drawable.bg_vertical_gradient)
        setHomeAsUpIndicator(TOOLBAR_INDICATOR_HAMBURGER, R.color.white)
        ivSearch.visible = true
        rvFoundList.visible = false
    }

    override fun onSearchOpen() {
        activity?.showKeyboard()
        setToolbarBackgroundColor(R.color.alto_alpha_35)
        setHomeAsUpIndicator(TOOLBAR_INDICATOR_HIDE)
        ivSearch.visible = false
        rvFoundList.visible = true
    }

    /**
     * view state
     */
    override fun onLoadListItems(items: ArrayList<GuestCard>) {}

    override fun onRefreshList() {}

    override fun onError() {}
}