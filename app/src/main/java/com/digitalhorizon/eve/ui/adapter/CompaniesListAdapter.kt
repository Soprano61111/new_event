package com.digitalhorizon.eve.ui.adapter

import android.annotation.SuppressLint
import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.mvp.model.Company
import com.digitalhorizon.eve.ui.fragment.CompaniesListFragment
import kotlinx.android.synthetic.main.list_item_companies.view.*

class CompaniesListAdapter(val context: Context) : RecyclerView.Adapter<CompaniesListAdapter.CompaniesListViewHolder>() {
    val dataset = ArrayList<Company>()
    val router = ApplicationWrapper.INSTANCE.getRouter()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompaniesListViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.list_item_companies, parent, false)
        return CompaniesListViewHolder(view)
    }

    override fun getItemCount() = dataset.size

    @SuppressLint("SetTextI18n")
    override fun onBindViewHolder(holder: CompaniesListViewHolder, position: Int) {
        val company = dataset[position]
        holder.companyName.text = company.companyName

        val innText = String.format(context.getString(R.string.edit_guest_card_inn_count), company.inn)
        holder.companyInn.text = innText

        holder.companyLayout.setOnClickListener {
            router.exitWithResult(CompaniesListFragment.SELECTED_RESULT, company)
        }
    }

    class CompaniesListViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        var companyLayout = item.companyLayout
        var companyName = item.companyName
        var companyInn = item.companyInn
    }
}