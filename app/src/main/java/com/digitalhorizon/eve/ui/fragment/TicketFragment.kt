package com.digitalhorizon.eve.ui.fragment

import android.Manifest
import android.annotation.SuppressLint
import android.content.BroadcastReceiver
import android.content.Context
import android.content.Intent
import android.content.IntentFilter
import android.graphics.Bitmap
import android.net.ConnectivityManager
import android.os.Bundle
import android.support.v4.content.LocalBroadcastManager
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.ActivityWrapper
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.common.FragmentWrapper
import com.digitalhorizon.eve.common.ScreenPool
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.presenter.TicketPresenter
import com.digitalhorizon.eve.mvp.view.TicketView
import com.digitalhorizon.eve.utils.ImageUtils
import com.pawegio.kandroid.visible
import com.squareup.picasso.Picasso
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_ticket.*
import kotlinx.android.synthetic.main.view_drawer_menu.*

class TicketFragment : FragmentWrapper(), TicketView {
    companion object {
        fun getInstance() = TicketFragment()
    }

    @InjectPresenter
    lateinit var presenter: TicketPresenter

    val broadCastReceiver = object : BroadcastReceiver() {
        override fun onReceive(contxt: Context?, intent: Intent?) {
            val cm = context?.getSystemService(Context.CONNECTIVITY_SERVICE) as? ConnectivityManager
            val activeNetwork = cm?.activeNetworkInfo
            loadingDialog(activeNetwork == null, R.string.dialog_no_internet)
            activeNetwork?.let {
                getTicketInfo()
            }
        }
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_ticket
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHomeAsUpIndicator(TOOLBAR_INDICATOR_HAMBURGER)
        val filters = IntentFilter()
        filters.addAction("android.net.conn.CONNECTIVITY_CHANGE")
        context?.registerReceiver(broadCastReceiver, filters)
    }

    override fun onPause() {
        context?.let {
            LocalBroadcastManager.getInstance(it).unregisterReceiver(broadCastReceiver)
        }
        super.onPause()
    }

    override fun setQrCode(bitmap: Bitmap) {
        qrCode.setImageBitmap(bitmap)
    }

    @SuppressLint("SetTextI18n")
    override fun setGuestAccount(card: GuestCard) {
        val avatarView = (activity as ActivityWrapper).mainAvatar
        val drawerMenuUserName = (activity as ActivityWrapper).mainUserName
        avatarView.visible = true
        drawerMenuUserName.visible = true

        responsible.text = getString(R.string.ticket_who_call, card.guestResponsible.firstName, card.guestResponsible.lastName)
        userName.text = getString(R.string.ticket_welcome, card.firstName, card.lastName)
        callButton.text = card.guestResponsible.phone
        drawerMenuUserName.text = getString(R.string.guest_fullname_blank,
                card.surname,
                card.firstName,
                card.lastName
        )

        card.photo?.pictureName?.let {
            Picasso.with(activity).load(ImageUtils.getImagePath(it)).into(avatarView)
        }

        callButton.setOnClickListener {
            card.guestResponsible.phone?.let {
                callResponsible(it)
            }
        }
    }

    fun getTicketInfo() {
        presenter.getQrCode()
    }

    private fun callResponsible(phone: String) {
        RxPermissions(this.requireActivity())
                .request(Manifest.permission.CALL_PHONE)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe { isGranted ->
                    if (isGranted) {
                        ApplicationWrapper.INSTANCE.getRouter().navigateTo(ScreenPool.CALL_PHONE_ACTIVITY, phone)
                    }
                }
    }

    override fun showLoadingDialog(isVisible: Boolean) {
        loadingDialog(isVisible)
    }
}