package com.digitalhorizon.eve.ui.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.FragmentWrapper
import com.digitalhorizon.eve.mvp.model.StructureUnitList
import com.digitalhorizon.eve.mvp.presenter.StructureUnitListPresenter
import com.digitalhorizon.eve.mvp.view.StructureUnitListView
import com.digitalhorizon.eve.ui.adapter.StructureUnitListAdapter
import com.paginate.Paginate
import kotlinx.android.synthetic.main.fragment_structure_unit_list.*

class StructureUnitListFragment : FragmentWrapper(), StructureUnitListView, Paginate.Callbacks {
    companion object {
        const val SELECTED_RESULT = 1

        fun getInstance() = StructureUnitListFragment()
    }

    @InjectPresenter
    lateinit var presenter: StructureUnitListPresenter

    private var adapter: StructureUnitListAdapter? = null
    private var paginate: Paginate? = null
    private var loading = false

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_structure_unit_list
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolbarBackgroundDrawable(R.drawable.bg_vertical_gradient)
        setHomeAsUpIndicator(TOOLBAR_INDICATOR_BACK_ARROW, R.color.white)
        setTitleColor(R.color.white)
        setTitle(getString(R.string.structure_unit_list_title))

        initListItems()

        swipe.setOnRefreshListener {
            swipe.isRefreshing = false
            presenter.pullToRefresh()
        }
    }

    private fun initListItems() {
        if (adapter == null) {
            adapter = StructureUnitListAdapter()
        }

        adapter?.dataset?.clear()
        structureUnitList.adapter = adapter
        structureUnitList.layoutManager = LinearLayoutManager(activity)

        if (!presenter.isLoadedAllItems()) {
            Paginate.with(structureUnitList, this).build()
        }
    }

    override fun onLoadMore() {
        loading = true
        presenter.loadStructureUnits()
    }

    override fun isLoading() = loading

    override fun hasLoadedAllItems(): Boolean {
        if (presenter.isLoadedAllItems()) {
            paginate?.unbind()
            return true
        }
        return false
    }

    override fun onLoadItems(items: StructureUnitList) {
        loading = false
        presenter.lastLoadedCount = items.items.size
        presenter.itemsPage++
        adapter?.dataset?.addAll(items.items)
        adapter?.notifyDataSetChanged()
    }

    override fun onReload() {
        presenter.itemsPage = 0
        presenter.lastLoadedCount = 0
        loading = false

        initListItems()
    }

    override fun showToast(resId: Int) {
        Toast.makeText(activity, getText(resId), Toast.LENGTH_LONG).show()
    }
}