package com.digitalhorizon.eve.ui.fragment

import android.os.Bundle
import android.view.View
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.*
import com.pawegio.kandroid.runDelayed

class SplashFragment : FragmentWrapper() {
    companion object {
        fun getInstance() = SplashFragment()
    }

    private val DELAY: Long = 1500

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_splash
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showToolbar(TOOLBAR_HIDE)

        runDelayed(DELAY){
            if (Settings.IsAuthorized) {
                ApplicationWrapper.INSTANCE.getRouter().newRootScreen(ScreenPool.MAIN_FRAGMENT)
            } else {
                ApplicationWrapper.INSTANCE.getRouter().newRootScreen(ScreenPool.REGISTRATION_FRAGMENT)
            }
        }
    }
}