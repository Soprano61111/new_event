package com.digitalhorizon.eve.ui.fragment.guestcard

import android.annotation.SuppressLint
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.transition.TransitionManager
import android.support.v7.app.AlertDialog
import android.view.View
import android.widget.TextView
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardState
import com.digitalhorizon.eve.mvp.model.Pass
import com.digitalhorizon.eve.mvp.presenter.guestcard.GuestCardForSecurityPresenter
import com.digitalhorizon.eve.mvp.view.guestcard.GuestCardForSecurityView
import com.digitalhorizon.eve.utils.ImageUtils
import com.pawegio.kandroid.visible
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_guest_card_security.*
import kotlinx.android.synthetic.main.view_guest_card_head.*

class GuestCardForSecurityFragment : BaseGuestCardFragment(), GuestCardForSecurityView {
    companion object {
        private const val KEY_PASS = "key_pass"
        private const val KEY_CARD = "key_card"

        fun getBundle(pass: Pass, card: GuestCard) : Bundle{
            val argument = Bundle()
            argument.putSerializable(KEY_PASS, pass)
            argument.putSerializable(KEY_CARD, card)
            return argument
        }

        fun getInstance(bundle: Bundle): GuestCardForSecurityFragment {
            val fragment = GuestCardForSecurityFragment()
            fragment.arguments = bundle
            return fragment
        }
    }

    @InjectPresenter
    lateinit var presenter: GuestCardForSecurityPresenter

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_guest_card_security
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showToolbar(TOOLBAR_OVERLAY)
        setHomeAsUpIndicator(TOOLBAR_INDICATOR_HAMBURGER)

        arguments?.let{ bundle ->
            val guestCard = bundle.get(KEY_CARD) as? GuestCard
            val guestPass = bundle.get(KEY_PASS) as? Pass

            guestCard?.let {
                showGuestCard(it)
            }
            guestCard?.let { presenter.getCountDeny(it) }
            passSend.setOnClickListener { guestPass?.let { presenter.passGuest(it) } }
            failSend.setOnClickListener { guestPass?.let { presenter.denyGuest(it) } }
        }

        guestPhoto.setOnClickListener {
            zoomGuestPhoto()
        }
    }

    override fun showGuestCard(guestCard: GuestCard) {
        meetingFullName.text = guestCard.accompanyings
        companyName.text = guestCard.company?.companyName
        guestPriority.text = guestCard.priority.name
        guestCard.state?.let { setState(it) }
        guestFullName.text = getString(
                R.string.guest_fullname_blank,
                guestCard.surname,
                guestCard.firstName,
                guestCard.lastName
        )
        guestCard.photo?.pictureName?.let {
            Picasso.with(context)
                    .load(ImageUtils.getImagePath(it))
                    .resize(guestCard.photo!!.width, guestCard.photo!!.height)
                    .into(guestPhoto)
        }
    }

    private fun zoomGuestPhoto() {
        val cs = ConstraintSet()
        val targetVisibility = if (infoLayout.visible) View.GONE else View.VISIBLE

        cs.clone(rootLayout)
        cs.setVisibility(R.id.infoLayout, targetVisibility)
        TransitionManager.beginDelayedTransition(rootLayout)
        cs.applyTo(rootLayout)
    }

    private fun setState(state: GuestCardState) {
        guestState.text = getText(state.strResId)

        when(state) {
            GuestCardState.AGREED -> guestStateIndicator.setImageResource(R.drawable.ic_approved_state)
            GuestCardState.REFUSED -> guestStateIndicator.setImageResource(R.drawable.ic_wrong)
            GuestCardState.SENDFORAPPROVAL -> guestStateIndicator.setImageResource(R.drawable.ic_red_dot)
            GuestCardState.SAVED -> guestStateIndicator.setImageResource(R.drawable.ic_saved_state)
            else -> {
            }
        }
    }

    @SuppressLint("SetTextI18n")
    override fun setDenyCount(count: Int) {
        if (count > 0) {
            refusedLabel.visibility = View.VISIBLE
            val text = String.format(getString(R.string.guest_deny), count)
            refusedText.text = text
        } else {
            refusedLabel.visibility = View.GONE
        }
    }

    override fun showErrorMessage(msg: Int) {
        context?.let {
            val dialog =  AlertDialog.Builder(it)
                    .setView(R.layout.dialog_error_message)
                    .setPositiveButton(R.string.ok) { _, _ ->

                    }
                    .create()

            dialog.findViewById<TextView>(R.id.textAlert)?.text = getText(msg)
            dialog.show()
        }
    }
}