package com.digitalhorizon.eve.ui.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.FragmentWrapper
import com.digitalhorizon.eve.mvp.model.CompaniesList
import com.digitalhorizon.eve.mvp.presenter.CompaniesListPresenter
import com.digitalhorizon.eve.mvp.view.CompaniesListView
import com.digitalhorizon.eve.ui.adapter.CompaniesListAdapter
import com.paginate.Paginate
import kotlinx.android.synthetic.main.fragment_companies_list.*

class CompaniesListFragment : FragmentWrapper(), CompaniesListView, Paginate.Callbacks {
    companion object {
        const val SELECTED_RESULT = 0

        fun getInstance() = CompaniesListFragment()
    }

    @InjectPresenter
    lateinit var presenter: CompaniesListPresenter

    private var adapter: CompaniesListAdapter? = null
    private var paginator: Paginate? = null
    private var loading = false

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_companies_list
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolbarBackgroundDrawable(R.drawable.bg_vertical_gradient)
        setHomeAsUpIndicator(TOOLBAR_INDICATOR_BACK_ARROW, R.color.white)
        setTitleColor(R.color.white)
        setTitle(getString(R.string.companies_list_title))

        initListItems()

        swipe.setOnRefreshListener {
            swipe.isRefreshing = false
            presenter.pullToRefresh()
        }
    }

    private fun initListItems() {
        if (adapter == null) {
            adapter = CompaniesListAdapter(activity!!)
        }

        adapter?.dataset?.clear()
        companiesList.adapter = adapter
        companiesList.layoutManager = LinearLayoutManager(activity)

        if (!presenter.isLoadedAllItems()) {
            Paginate.with(companiesList, this).build()
        }
    }

    override fun onLoadMore() {
        loading = true
        presenter.loadCompanies()
    }

    override fun isLoading() = loading

    override fun hasLoadedAllItems(): Boolean {
        if (presenter.isLoadedAllItems()) {
            paginator?.unbind()
            return true
        }
        return false
    }

    override fun onLoadItems(items: CompaniesList) {
        loading = false
        presenter.lastLoadedCount = items.items.size
        presenter.itemsPage++
        adapter?.dataset?.addAll(items.items)
        adapter?.notifyDataSetChanged()
    }

    override fun onReload() {
        presenter.itemsPage = 0
        presenter.lastLoadedCount = 0
        loading = false

        initListItems()
    }

    override fun showToast(resId: Int) {
        Toast.makeText(activity, getText(resId), Toast.LENGTH_LONG).show()
    }
}