package com.digitalhorizon.eve.ui.widget

import android.app.AlertDialog
import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.widget.Button
import android.widget.FrameLayout
import android.widget.TextView
import com.digitalhorizon.eve.R
import com.pawegio.kandroid.find
import com.pawegio.kandroid.visible

class CustomDialog(val context: Context) {
    interface OnSelectedPositiveListener {
        fun onSelectedItem()
    }

    interface OnSelectedNegativeListener {
        fun onSelectedItem()
    }

    private var dialog: AlertDialog? = null
    private var dialogView: View? = null
    private var btnPositive: Button? = null
    private var btnNegative: Button? = null

    init {
        dialogView = LayoutInflater.from(context).inflate(R.layout.dialog_custom, null)
        dialogView?.find<FrameLayout>(R.id.wrapper)?.clipToOutline = true
        btnPositive = dialogView?.findViewById(R.id.btnPositive)
        btnNegative = dialogView?.findViewById(R.id.btnNegative)

        dialog = AlertDialog.Builder(context, R.style.AlertDialog)
                .setView(dialogView)
                .create()
    }

    fun setPositiveClickListeners(onPositiveListener: OnSelectedPositiveListener): CustomDialog {
        btnPositive?.setOnClickListener { onPositiveListener.onSelectedItem() }
        dialog?.dismiss()
        return this
    }

    fun setNegativeClickListeners(onNegativeListener: OnSelectedNegativeListener): CustomDialog {
        btnNegative?.setOnClickListener {
            onNegativeListener.onSelectedItem()
            dialog?.dismiss()
        }
        return this
    }

    fun setNamePositiveButton(positiveBtnNameResId: Int): CustomDialog {
        btnPositive?.text = context.getString(positiveBtnNameResId)
        return this
    }

    fun setNamePositiveButton(positiveBtnName: String): CustomDialog {
        btnPositive?.text = positiveBtnName
        return this
    }

    fun setNameNegativeButton(negativeBtnNameResId: Int): CustomDialog {
        btnNegative?.text = context.getString(negativeBtnNameResId)
        return this
    }

    fun setNameNegativeButton(negativeBtnName: String): CustomDialog {
        btnNegative?.text = negativeBtnName
        return this
    }

    /**
     *   If you are using "one button mode", then set "positive button" listener
     */

    fun setOneButtonMode(isOneButton: Boolean): CustomDialog {
        btnNegative?.visible = isOneButton
        return this
    }

    fun setTitle(str: String): CustomDialog {
        dialogView?.find<TextView>(R.id.titleText)?.text = str
        return this
    }

    fun setTitle(strResId: Int): CustomDialog {
        dialogView?.find<TextView>(R.id.titleText)?.text = context.getString(strResId)
        return this
    }

    fun dismiss() {
        dialog?.dismiss()
    }

    fun show() {
        dialog?.show()
    }
}