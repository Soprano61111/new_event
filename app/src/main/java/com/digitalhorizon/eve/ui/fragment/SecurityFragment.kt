package com.digitalhorizon.eve.ui.fragment

import android.annotation.SuppressLint
import android.os.Bundle
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.ActivityWrapper
import com.digitalhorizon.eve.common.FragmentWrapper
import com.digitalhorizon.eve.common.ScreenPool
import com.digitalhorizon.eve.mvp.model.Pass
import com.digitalhorizon.eve.mvp.model.StafferCard
import com.digitalhorizon.eve.mvp.presenter.SecurityPresenter
import com.digitalhorizon.eve.mvp.view.SecurityView
import com.digitalhorizon.eve.ui.activity.ScannerActivity
import com.digitalhorizon.eve.utils.ImageUtils
import com.pawegio.kandroid.runDelayed
import com.pawegio.kandroid.visible
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_security.*
import kotlinx.android.synthetic.main.view_drawer_menu.*

class SecurityFragment : FragmentWrapper(), SecurityView {
    companion object {
        fun getInstance() = SecurityFragment()
    }

    @InjectPresenter
    lateinit var presenter: SecurityPresenter

    private val SHOWING_NOT_FOUND_TIME: Long = 3000

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_security
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getProfile()

        setHomeAsUpIndicator(TOOLBAR_INDICATOR_HAMBURGER)
        startScan.setOnClickListener {
            presenter.router.navigateTo(ScreenPool.SCANNER_ACTIVITY)
        }

        presenter.router.setResultListener(ScannerActivity.SCANNER_REQUEST_CODE) { result ->
            if (result != null && result is String) {
                presenter.getGuestCardData(Pass(result))
            }
        }
    }

    override fun showViewGuestNotFound() {
        wellcomeGroup.visible = false
        layoutGuestNotFound.visible = true
        runDelayed(SHOWING_NOT_FOUND_TIME) {
            wellcomeGroup.visible = true
            layoutGuestNotFound.visible = false
        }
    }

    override fun progressBar(isVisible: Boolean) {
        progressBarSecurity.visible = isVisible
    }

    @SuppressLint("SetTextI18n")
    override fun setSecurityCard(card: StafferCard) {
        val avatarView = (activity as ActivityWrapper).mainAvatar
        val drawerMenuUserName = (activity as ActivityWrapper).mainUserName

        avatarView.visible = true
        drawerMenuUserName.visible = true

        drawerMenuUserName.text = "${card.surname}\n${card.name} ${card.additionalName}"
        goodDayText.text = getString(R.string.security_good_day, card.name)

        card.photo?.pictureName?.let {
            Picasso.with(activity).load(ImageUtils.getImagePath(it)).into(avatarView)
        }
    }
}