package com.digitalhorizon.eve.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.mvp.model.StructureUnit
import com.digitalhorizon.eve.ui.fragment.StructureUnitListFragment
import kotlinx.android.synthetic.main.list_item_structure_unit.view.*

class StructureUnitListAdapter : RecyclerView.Adapter<StructureUnitListAdapter.StructureListViewHolder>() {
    val dataset = ArrayList<StructureUnit>()
    val router = ApplicationWrapper.INSTANCE.getRouter()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): StructureListViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.list_item_structure_unit, parent, false)
        return StructureListViewHolder(view)
    }

    override fun getItemCount() = dataset.size

    override fun onBindViewHolder(holder: StructureListViewHolder, position: Int) {
        val structureUnit = dataset[position]
        holder.structureUnitName.text = structureUnit.name

        holder.structureUnitLayout.setOnClickListener {
            router.exitWithResult(StructureUnitListFragment.SELECTED_RESULT, structureUnit)
        }
    }

    class StructureListViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        var structureUnitLayout = item.structureUnitLayout
        var structureUnitName = item.structureUnitName
    }
}