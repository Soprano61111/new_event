package com.digitalhorizon.eve.ui.fragment

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.Gravity
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.FragmentWrapper
import com.digitalhorizon.eve.mvp.model.CardsList
import com.digitalhorizon.eve.mvp.model.PurposeEnum
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardPriority
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardState
import com.digitalhorizon.eve.mvp.presenter.GuestsListPresenter
import com.digitalhorizon.eve.mvp.view.GuestsListView
import com.digitalhorizon.eve.ui.adapter.GuestsListAdapter
import com.digitalhorizon.eve.ui.widget.ApproveDialog
import com.digitalhorizon.eve.ui.widget.CustomToast
import com.digitalhorizon.eve.ui.widget.DeclineDialog
import com.paginate.Paginate
import com.pawegio.kandroid.fromApi
import kotlinx.android.synthetic.main.fragment_guests_list.*

class GuestsListFragment : FragmentWrapper(), GuestsListView, Paginate.Callbacks, FragmentLifeCycle {
    companion object {
        private const val ASSIGNED_KEY = "assigned_key"
        private const val PURPOSE_KEY = "purpose_key"

        fun getInstance(/*assigned: Int, purposeEnum: PurposeEnum*/): GuestsListFragment {
            val fragment = GuestsListFragment()
//            val bundle = Bundle()
//            bundle.putSerializable(ASSIGNED_KEY, assigned)
//            bundle.putSerializable(PURPOSE_KEY, purposeEnum)
//            fragment.arguments = bundle
            return fragment
        }
    }

    @InjectPresenter
    lateinit var presenter: GuestsListPresenter

    private var adapter: GuestsListAdapter? = null
    private val DIALOG_REJECT = "dialog_reject"
    private val DIALOG_APPROVE = "dialog_approve"
    private var loading = false
    private var paginator: Paginate? = null

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_guests_list
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        showToolbar(TOOLBAR_HIDE)

        initListItems()

        swipe.setOnRefreshListener {
            swipe.isRefreshing = false
            presenter.refresh()
        }
    }

    override fun fragmentOnResume() {
        if (isAdded && presenter.isFiltersChanged) {
            presenter.refresh()
        }
    }

    override fun fragmentOnPause() {

    }

    fun setStateFilters(states: ArrayList<GuestCardState>, priorities: ArrayList<Int>) {
        presenter.isFiltersChanged = true
        presenter.stateFilters = states
        presenter.priorityFilters = priorities
    }

    private fun onChangedGuestCardState(guestCard: GuestCard, state: GuestCardState) {
        val toast = CustomToast.imgWithTextToast(activity, guestCard, state)
        toast.setGravity(Gravity.TOP, 0, 0)
        toast.show()

        adapter?.removeItem(guestCard)
    }

    private fun initListItems() {
        if (adapter == null) {
            adapter = GuestsListAdapter(activity, presenter)
        }

        guestsList.adapter = adapter
        guestsList.layoutManager = LinearLayoutManager(activity)

        if (!presenter.isLoadedAllItems()) {
            Paginate.with(guestsList, this).build()
        }
    }

    override fun onLoadMore() {
        loading = true
        presenter.getGuestCards()
    }

    override fun isLoading() = loading

    override fun hasLoadedAllItems(): Boolean {
        if (presenter.isLoadedAllItems()) {
            paginator?.unbind()
            return true
        }
        return false
    }

    /**
     * view state
     */
    override fun showToast(resId: Int) {
        Toast.makeText(activity, getText(resId), Toast.LENGTH_LONG).show()
    }

    override fun showRejectDialog(guestCard: GuestCard) {
        val myDialogFragment = DeclineDialog.getInstance(R.layout.dialog_decline_pressed, guestCard, R.string.dialog_title_decline,
                object : DeclineDialog.ReadyListener {
                    override fun ready(refused: String, guestCard: GuestCard) {
                        presenter.rejectGuestCard(guestCard, refused)
                    }
                })
        myDialogFragment.show(childFragmentManager, DIALOG_REJECT)
    }

    override fun showApproveDialog(guestCard: GuestCard) {
        val dialog = ApproveDialog.getInstance(guestCard)
        dialog.listener = object : ApproveDialog.OnSelectedListener {
            override fun onSelected(card: GuestCard, priority: GuestCardPriority) {
                presenter.approveGuestCard(card, priority)
            }
        }
        dialog.show(childFragmentManager, DIALOG_APPROVE)
    }

    override fun onRejectGuestCard(guestCard: GuestCard) {
        onChangedGuestCardState(guestCard, GuestCardState.REFUSED)
    }

    override fun onApproveGuestCard(guestCard: GuestCard) {
        onChangedGuestCardState(guestCard, GuestCardState.AGREED)
    }

    override fun onReload() {
        presenter.itemsPage = 0
        presenter.lastLoadedCount = 0
        loading = false

        initListItems()
    }

    override fun onLoadItems(items: CardsList) {
        loading = false
        presenter.lastLoadedCount = items.items.size
        presenter.itemsPage++
        adapter?.dataset?.addAll(items.items)
        adapter?.notifyDataSetChanged()
    }
}