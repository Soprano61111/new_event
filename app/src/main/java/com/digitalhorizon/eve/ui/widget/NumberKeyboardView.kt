package com.digitalhorizon.eve.ui.widget

import android.content.Context
import android.graphics.PorterDuff
import android.support.constraint.ConstraintLayout
import android.support.v4.content.ContextCompat
import android.util.AttributeSet
import android.view.View
import android.widget.Button
import android.widget.EditText
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.utils.inflate
import kotlinx.android.synthetic.main.view_number_keyboard.view.*

class NumberKeyboardView(context: Context, attrs: AttributeSet) : ConstraintLayout(context, attrs) {
    private var editText: EditText? = null
    private val listBtnNumber: ArrayList<Button> = ArrayList()

    init {
        this.context.inflate(R.layout.view_number_keyboard, this)
        listBtnNumber.add(btnOne)
        listBtnNumber.add(btnTwo)
        listBtnNumber.add(btnThree)
        listBtnNumber.add(btnFour)
        listBtnNumber.add(btnFive)
        listBtnNumber.add(btnSix)
        listBtnNumber.add(btnSeven)
        listBtnNumber.add(btnEight)
        listBtnNumber.add(btnNine)
        listBtnNumber.add(btnZero)
    }

    override fun onFinishInflate() {
        super.onFinishInflate()

        listBtnNumber.forEach { button ->
            button.setOnClickListener {onClickNumber(it)}
        }

        btnBackspace.setOnClickListener {onClickNumber(it)}
    }

    fun attachEditText(editText: EditText){
        this.editText = editText
    }

    private fun onClickNumber(view: View){
        if (!isEnabled) {
            return
        }

        val text = editText?.text

        when(view.id){
            R.id.btnOne -> editText?.setText("${text}1")
            R.id.btnTwo -> editText?.setText("${text}2")
            R.id.btnThree -> editText?.setText("${text}3")
            R.id.btnFour -> editText?.setText("${text}4")
            R.id.btnFive -> editText?.setText("${text}5")
            R.id.btnSix -> editText?.setText("${text}6")
            R.id.btnSeven -> editText?.setText("${text}7")
            R.id.btnEight -> editText?.setText("${text}8")
            R.id.btnNine -> editText?.setText("${text}9")
            R.id.btnZero -> editText?.setText("${text}0")
            R.id.btnBackspace -> {
                text?.let {
                    if (it.isNotEmpty()){
                        it.delete(it.length - 1, it.length)
                    }
                }
            }
        }
    }

    fun isKeyboardEnabled(isEnabled: Boolean) {
        this.isEnabled = isEnabled
        val color = if (isEnabled) R.color.colorPrimaryDark else R.color.colorPrimaryLight

        listBtnNumber.forEach { button ->
            button.isEnabled = isEnabled
        }

        btnBackspace.isEnabled = isEnabled
        btnBackspace.setColorFilter(ContextCompat.getColor(context, color), PorterDuff.Mode.SRC_IN)
    }
}