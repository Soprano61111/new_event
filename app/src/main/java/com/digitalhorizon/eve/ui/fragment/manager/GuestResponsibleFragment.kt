package com.digitalhorizon.eve.ui.fragment.manager

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.ActivityWrapper
import com.digitalhorizon.eve.common.FragmentWrapper
import com.digitalhorizon.eve.mvp.presenter.manager.GuestResponsiblePresenter
import com.digitalhorizon.eve.mvp.view.manager.GuestResponsibleView
import com.digitalhorizon.eve.ui.fragment.StubRolesFragment
import kotlinx.android.synthetic.main.fragment_base_guest_manager.*
import kotlinx.android.synthetic.main.view_guests_items.*

class GuestResponsibleFragment : BaseManagerFragment(), GuestResponsibleView {
    companion object {
        fun getInstance() = GuestResponsibleFragment()
    }

    @InjectPresenter
    lateinit var presenter: GuestResponsiblePresenter

    private val MY_GUESTS_TAB = 0
    private val I_AM_INTERESTED_TAB = 1

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hamburgerClickListeners()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        setHamburgerForRole()
    }

    override fun getFragmentList(): ArrayList<Fragment> {
        return arrayListOf(
                StubRolesFragment.getInstance(),
                StubRolesFragment.getInstance()
        )
    }

    override fun prepareTabLayout() {
        tabLayout.getTabAt(MY_GUESTS_TAB)?.text = getText(R.string.base_guest_manager_my_guests_tab)
        tabLayout.getTabAt(I_AM_INTERESTED_TAB)?.text = getText(R.string.base_guest_manager_i_am_interested_tab)
    }

    private fun hamburgerClickListeners() {
        val fragmentPagerAdapter = viewPager.adapter
        (activity as ActivityWrapper).navItemInvited.setOnClickListener {
            FragmentWrapper.isInvitedMode = true
            fragmentPagerAdapter?.notifyDataSetChanged()
            prepareTabLayout()
        }
    }
}