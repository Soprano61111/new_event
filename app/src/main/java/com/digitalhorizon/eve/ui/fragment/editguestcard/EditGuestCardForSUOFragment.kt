package com.digitalhorizon.eve.ui.fragment.editguestcard

import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.view.View
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardState.AGREED
import kotlinx.android.synthetic.main.fragment_edit_guest_card.*

class EditGuestCardForSUOFragment : EditGuestCardFragment() {
    companion object {
        private const val DATA_KEY = "data_key"
        fun getInstance(guestCard: GuestCard?): EditGuestCardForSUOFragment {
            val fragment = EditGuestCardForSUOFragment()
            guestCard?.let {
                val arguments = Bundle()
                arguments.putSerializable(DATA_KEY, guestCard)
                fragment.arguments = arguments
            }
            return fragment
        }
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setToolbarBackgroundDrawable(R.drawable.bg_vertical_gradient)
        setHomeAsUpIndicator(TOOLBAR_INDICATOR_BACK_ARROW, R.color.white)
        setTitleColor(R.color.white)
        presenter.card = arguments?.getSerializable(DATA_KEY) as? GuestCard

        setCardFields()
        checkEditPermission()
    }

    private fun checkEditPermission() {
        if (presenter.card?.state == AGREED) {
            addPhoto.isEnabled = false
            surName.isEnabled = false
            firstName.isEnabled = false
            lastName.isEnabled = false
            companyLayout.isEnabled = false
            companyIcon.setColorFilter(ContextCompat.getColor(context!!, R.color.white))
            email.isEnabled = false
            position.isEnabled = false
            countryCodePicker.setCcpClickable(false)
            phone.isEnabled = false
            priority.isEnabled = true
            responsible.isEnabled = true
            accompanyings.isEnabled = false
        }
        applicantLayout.isEnabled = false
        applicantIcon.setColorFilter(ContextCompat.getColor(context!!, R.color.white))
    }
}