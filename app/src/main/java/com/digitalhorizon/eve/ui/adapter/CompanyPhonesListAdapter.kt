package com.digitalhorizon.eve.ui.adapter

import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.mvp.model.PhonesCompany
import kotlinx.android.synthetic.main.list_item_phone_company.view.*

class CompanyPhonesListAdapter : RecyclerView.Adapter<CompanyPhonesListAdapter.CompanyPhonesListViewHolder>() {
    var dataset = ArrayList<PhonesCompany>()

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): CompanyPhonesListViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.list_item_phone_company, parent, false)
        return CompanyPhonesListViewHolder(view)
    }

    override fun getItemCount() = dataset.size

    override fun onBindViewHolder(holder: CompanyPhonesListViewHolder, position: Int) {
        val phone = dataset[position]
        holder.phoneCompany.setText(phone.phoneNumber)
        holder.phoneCompanyRemove.setOnClickListener { removeItem(phone) }
    }

    fun removeItem(phone: PhonesCompany) {
        val position = dataset.indexOf(phone)
        dataset.removeAt(position)
        notifyDataSetChanged()
    }

    class CompanyPhonesListViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        var phoneCompany = item.phoneCompany
        var phoneCompanyAdditional = item.phoneCompanyAdditional
        var phoneCompanyRemove = item.phoneCompanyRemove
    }
}