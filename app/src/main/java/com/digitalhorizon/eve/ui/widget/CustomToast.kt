package com.digitalhorizon.eve.ui.widget

import android.content.Context
import android.view.View
import android.widget.TextView
import android.widget.Toast
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardState
import com.digitalhorizon.eve.utils.ImageUtils
import com.squareup.picasso.Picasso

object CustomToast {
    fun imgWithTextToast(context: Context?, card: GuestCard, state: GuestCardState): Toast {
        val toast = Toast(context)
        toast.view = View.inflate(context, R.layout.view_toast_img_text, null)
        toast.duration = Toast.LENGTH_LONG

        val avatar = toast.view.findViewById<de.hdodenhof.circleimageview.CircleImageView>(R.id.avatar)
        val stateIndicator = toast.view.findViewById<de.hdodenhof.circleimageview.CircleImageView>(R.id.stateIndicator)
        val tvState = toast.view.findViewById<TextView>(R.id.tvState)
        val fullName = toast.view.findViewById<TextView>(R.id.fullName)
        val priority = toast.view.findViewById<TextView>(R.id.priority)

        fullName.text = "${card.firstName} ${card.surname} ${card.lastName}"
        priority.text = card.priority?.name

        card.photo?.pictureName?.let {
            Picasso.with(context)
                    .load(ImageUtils.getImagePath(it))
                    .centerCrop()
                    .resizeDimen(R.dimen.view_header_avatar_size, R.dimen.view_header_avatar_size)
                    .into(avatar)
        }

        tvState.text = context?.getText(state.strResId)

        when(state) {
            GuestCardState.AGREED -> {
                stateIndicator.setImageResource(R.drawable.ic_approved_state)
            }
            GuestCardState.CHANGEREQUEST -> {
                stateIndicator.setImageResource(R.drawable.ic_red_dot)
            }
            GuestCardState.DELETED -> {
                stateIndicator.setImageResource(R.drawable.ic_wrong)
            }
            GuestCardState.REFUSED -> {
                stateIndicator.setImageResource(R.drawable.ic_wrong)
            }
            GuestCardState.SENDFORAPPROVAL -> {
                stateIndicator.setImageResource(R.drawable.ic_red_dot)
            }
            else -> {
                stateIndicator.setImageResource(R.drawable.ic_saved_state)
            }
        }

        return toast
    }
}