package com.digitalhorizon.eve.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.Settings
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardState
import com.digitalhorizon.eve.mvp.model.Events
import kotlinx.android.synthetic.main.list_item_filter.view.*
import org.greenrobot.eventbus.EventBus

class FilterAdapterState(val context: Context?) : RecyclerView.Adapter<FilterAdapterState.FilterViewHolder>() {

    val map = LinkedHashMap<GuestCardState, Boolean>()
    val arrayGuestCardStateName = ArrayList<GuestCardState>()
    val arrayGuestCardStateBoolean = ArrayList<Boolean>()
    val finalGustCardState = ArrayList<GuestCardState>()
    private val SAVE_FILTER_STATE = "save_filter_state"

    init {
        //load to file
        val newmap = context?.let { Settings.LoadLinkedHashMapFromInternalStorage(SAVE_FILTER_STATE, it) }
        //если в файле не пусто то копируем
        if (newmap?.size != 0) {
            map.clear()
            newmap?.let { map.putAll(it) }
        }
        //если в файле пусто то заполняем
        else {
            map[GuestCardState.AGREED] = false
            map[GuestCardState.SENDFORAPPROVAL] = false
            map[GuestCardState.REFUSED] = false
            map[GuestCardState.SAVED] = false
            map[GuestCardState.DELETED] = false
            map[GuestCardState.CHANGEREQUEST] = false
        }

        for (entry in map) {
            val key = entry.key
            val tab = entry.value
            arrayGuestCardStateName.add(key)
            arrayGuestCardStateBoolean.add(tab)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_filter, parent, false)
        return FilterViewHolder(view)
    }

    override fun onBindViewHolder(holder: FilterViewHolder, position: Int) {

        holder.button.text = null
        holder.button.textOn = null
        holder.button.textOff = null
        holder.button.setSingleLine(true)
        holder.button.text = context?.getString(arrayGuestCardStateName[position].strResId)
        if (arrayGuestCardStateBoolean.get(position)) {
            holder.button.setChecked(true)
        } else {
            holder.button.setChecked(false)
        }
        holder.button.setOnClickListener {
            finalGustCardState.clear()
            if (arrayGuestCardStateBoolean.get(position)) {
                arrayGuestCardStateBoolean[position] = false
                holder.button.setChecked(false)
            } else {
                arrayGuestCardStateBoolean[position] = true
                holder.button.setChecked(true)
            }
            for (i in 0 until arrayGuestCardStateBoolean.size) {
                if (arrayGuestCardStateBoolean.get(i)) {
                    finalGustCardState.add(arrayGuestCardStateName.get(i))
                }
            }

            for (i in 0 until arrayGuestCardStateName.size) {
                map.put(arrayGuestCardStateName.get(position), arrayGuestCardStateBoolean[position])
            }
            EventBus.getDefault().post(Events.State(finalGustCardState))
            context?.let { it1 -> Settings.SaveLinkedHashMapToInternalStorage(SAVE_FILTER_STATE, map, it1) }
        }
    }

    override fun getItemCount(): Int {
        return map.size
    }

    class FilterViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val button = item.title
    }
}