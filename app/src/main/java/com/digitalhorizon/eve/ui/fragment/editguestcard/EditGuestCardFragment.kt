package com.digitalhorizon.eve.ui.fragment.editguestcard

import android.Manifest
import android.content.Intent
import android.os.Bundle
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.view.View
import android.widget.ArrayAdapter
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.AccountManager
import com.digitalhorizon.eve.common.FragmentWrapper
import com.digitalhorizon.eve.common.ScreenPool
import com.digitalhorizon.eve.mvp.model.*
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardRequest
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardState
import com.digitalhorizon.eve.mvp.presenter.EditGuestCardPresenter
import com.digitalhorizon.eve.mvp.view.EditGuestCardView
import com.digitalhorizon.eve.ui.adapter.CompanyPhonesListAdapter
import com.digitalhorizon.eve.ui.fragment.CompaniesListFragment
import com.digitalhorizon.eve.ui.fragment.StructureUnitListFragment
import com.digitalhorizon.eve.utils.ImageUtils
import com.pawegio.kandroid.d
import com.pawegio.kandroid.runDelayed
import com.pawegio.kandroid.visible
import com.squareup.picasso.Picasso
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_edit_guest_card.*
import pl.aprilapps.easyphotopicker.DefaultCallback
import pl.aprilapps.easyphotopicker.EasyImage
import java.io.File

open class EditGuestCardFragment : FragmentWrapper(), EditGuestCardView {
    companion object {
        var responsibleList: ArrayList<GuestResponsible>? = null
    }

    @InjectPresenter
    lateinit var presenter: EditGuestCardPresenter

    private var isPhoneValid: Boolean = false
    private var companyPhonesListAdapter: CompanyPhonesListAdapter? = null

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_edit_guest_card
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)

        presenter.cardRequest = GuestCardRequest()

        presenter.router.setResultListener(CompaniesListFragment.SELECTED_RESULT) {
            val company = it as Company
            presenter.card?.company = company
        }

        presenter.router.setResultListener(StructureUnitListFragment.SELECTED_RESULT) {
            val structureUnit = it as StructureUnit
            presenter.card?.structureUnit = structureUnit
            presenter.cardRequest?.structureUnit?.id = structureUnit.id
        }

        setTextWatcher()

        countryCodePicker.registerCarrierNumberEditText(phone)
        countryCodePicker.setPhoneNumberValidityChangeListener { isValid ->
            isPhoneValid = isValid
            showErrorMessage(!isValid)
        }

        //TODO add save function
        saveBtn.setOnClickListener { saveGuestCard() }
        addPhone.setOnClickListener { addPhoneField() }
        addPhoto.setOnClickListener { checkPermission() }
    }

    fun setCardFields() {
        presenter.card?.let {
            setTitle(getString(R.string.edit_guest_card_title))
            setState(it.state)
            priorityText.text = it.priority.name
            surName.setText(it.surname)
            firstName.setText(it.firstName)
            lastName.setText(it.lastName)
            email.setText(it.email)
            position.setText(it.position)
            accompanyings.setText(it.accompanyings)
            applicantText.text = it.structureUnit.name
            setPriority()
            setCompany(it)
            countryCodePicker.fullNumber = it.phone
            setIconColor()

            setResponsible(null)
            presenter.getResponsible()

            companyLayout.setOnClickListener { _ ->
                presenter.router.navigateTo(ScreenPool.COMPANIES_LIST_FARGMENT)
            }

            applicantLayout.setOnClickListener { _ ->
                presenter.router.navigateTo(ScreenPool.STRUCTURE_UNIT_LIST_FRAGMENT)
            }

            presenter.photoFile?.let {
                setPhoto(null, it)
            } ?: run {
                setPhoto(presenter.card, null)
            }

            presenter.cardRequest?.structureUnit?.id = it.structureUnit.id
        }
    }

    private fun setState(state: GuestCardState) {
        stateText.text = getText(state.strResId)

        when (state) {
            GuestCardState.AGREED -> stateIndicator.setImageResource(R.drawable.ic_approved_state)
            GuestCardState.REFUSED -> stateIndicator.setImageResource(R.drawable.ic_wrong)
            GuestCardState.SENDFORAPPROVAL -> stateIndicator.setImageResource(R.drawable.ic_red_dot)
            GuestCardState.SAVED -> stateIndicator.setImageResource(R.drawable.ic_saved_state)
            else -> {
            }
        }
    }

    private fun setPhoto(card: GuestCard?, file: File?) {
        val picasso = Picasso.with(context)

        card?.photo?.pictureName?.let {
            picasso.load(ImageUtils.getImagePath(it))
                    .resize(card.photo!!.width, card.photo!!.height)
                    .into(photo)
        } ?: run {
            file?.let {
                picasso.load(it)
                        .into(photo)
            }
        }
    }

    private fun setPriority() {
        val arrayList = ArrayList<String>()
        val adapter = ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, arrayList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        AccountManager.getPriorities()
                .subscribe({
                    it.forEach { priority ->
                        //TODO Refactoring, fix duplicate position
                        if (!arrayList.contains(priority.name)) {
                            arrayList.add(priority.name)
                        }
                    }
                    var priorityName = ""
                    presenter.card?.priority?.let {
                        priorityName = it.name
                    }
                    val result = arrayList.find { it == priorityName }
                    val position = arrayList.indexOf(result)
                    priority.adapter = adapter
                    priority.setSelection(position)
                }, { err ->
                    d("Get Priority error")
                })
    }

    override fun setResponsible(list: ResponsibleList?) {
        val arrayList = ArrayList<String>()
        val adapter = ArrayAdapter<String>(activity, android.R.layout.simple_spinner_item, arrayList)
        adapter.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

        list?.let {
            it.items.forEach { responsible ->
                arrayList.add("${responsible.surName} ${responsible.firstName} ${responsible.lastName}")
            }
            if (arrayList.size > 0) {
                //for fix select responsible
                var fullName = ""
                presenter.card?.guestResponsible?.let {
                    fullName = "${it.surName} ${it.firstName} ${it.lastName}"
                }
                val result = arrayList.find { it == fullName }
                val position = arrayList.indexOf(result)
                responsible.adapter = adapter
                //fix select responsible
                responsible.setSelection(position)
            }
        } ?: run {
            presenter.card?.guestResponsible?.let {
                arrayList.add("${it.surName} ${it.firstName} ${it.lastName}")
                responsible.adapter = adapter
                presenter.cardRequest?.guestResponsible?.id = it.id
            }
        }
    }

    private fun setCompany(card: GuestCard) {
        card.company?.let {
            presenter.cardRequest?.company?.companyId = it.companyId
            companyText.text = it.companyName
            //fix display inn field
            runDelayed(500) {
                innField?.setText(it.inn)
            }
        }

        initCompanyPhonesListItems()
    }

    private fun setIconColor() {
        applicantIcon.setColorFilter(ContextCompat.getColor(context!!, R.color.black))
        companyIcon.setColorFilter(ContextCompat.getColor(context!!, R.color.black))
        responsibleIcon.setColorFilter(ContextCompat.getColor(context!!, R.color.black))
        priorityIcon.setColorFilter(ContextCompat.getColor(context!!, R.color.black))
    }

    override fun showToast(resId: Int) {
        Toast.makeText(activity, getText(resId), Toast.LENGTH_LONG).show()
    }

    private fun checkFieldsValidation() {
        saveBtn.isEnabled = !companyText.text.isNullOrEmpty() &&
                !innField.text.isNullOrEmpty() &&
                !position.text.isNullOrEmpty() &&
                priority.selectedItem != null &&
                !applicantText.text.isNullOrEmpty() &&
                responsible.selectedItem != null &&
                !surName.text.isNullOrEmpty() &&
                !firstName.text.isNullOrEmpty() &&
                !lastName.text.isNullOrEmpty() &&
                !email.text.isNullOrEmpty() &&
                !phone.text.isNullOrEmpty() &&
                isPhoneValid
    }

    private fun setTextWatcher() {
        val textWatcher = object : TextWatcher {
            override fun afterTextChanged(s: Editable?) {
                checkFieldsValidation()
            }

            override fun beforeTextChanged(s: CharSequence?, start: Int, count: Int, after: Int) {}
            override fun onTextChanged(s: CharSequence?, start: Int, before: Int, count: Int) {}
        }

        companyText.addTextChangedListener(textWatcher)
        innField.addTextChangedListener(textWatcher)
        position.addTextChangedListener(textWatcher)
        applicantText.addTextChangedListener(textWatcher)
        phone.addTextChangedListener(textWatcher)
        surName.addTextChangedListener(textWatcher)
        firstName.addTextChangedListener(textWatcher)
        lastName.addTextChangedListener(textWatcher)
        email.addTextChangedListener(textWatcher)
    }

    private fun showErrorMessage(isShowed: Boolean) {
        errorMsg.visible = isShowed
    }

    private fun initCompanyPhonesListItems() {
        if (companyPhonesListAdapter == null) {
            companyPhonesListAdapter = CompanyPhonesListAdapter()
        }

        presenter.card?.company?.phones?.let {
            companyPhonesListAdapter?.dataset?.apply {
                clear()
                addAll(it)
            }
        }

        phonesCompanyList.adapter = companyPhonesListAdapter
        phonesCompanyList.layoutManager = LinearLayoutManager(activity)
    }

    private fun addPhoneField() {
        val phone = PhonesCompany()
        companyPhonesListAdapter?.dataset?.add(phone)
        companyPhonesListAdapter?.notifyDataSetChanged()
    }

    private fun openGallery() {
        EasyImage.openGallery(this, 0)
    }

    override fun onActivityResult(requestCode: Int, resultCode: Int, data: Intent?) {
        super.onActivityResult(requestCode, resultCode, data)

        EasyImage.handleActivityResult(requestCode, resultCode, data, activity, object : DefaultCallback() {
            override fun onImagePicked(imageFile: File?, source: EasyImage.ImageSource?, type: Int) {
                presenter.photoFile = imageFile
                setPhoto(null, imageFile)
            }
        })
    }

    private fun checkPermission() {
        activity?.let {
            RxPermissions(it)
                    .request(Manifest.permission.READ_EXTERNAL_STORAGE)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { isGranted ->
                        if (isGranted) {
                            openGallery()
                        } else {
                            showToast(R.string.permission_read_external_storage)
                        }
                    }
        }
    }

    private fun saveGuestCard() {
        presenter.photoFile?.let {
            presenter.savePhoto()
        } ?: run {
            getCardFields()
            presenter.saveCard()
        }
    }

    override fun getCardFields() {
        presenter.cardRequest?.let {
            it.surName = surName.text.toString()
            it.firstName = firstName.text.toString()
            it.lastName = lastName.text.toString()
            it.email = email.text.toString()
            it.position = position.text.toString()
            it.phone = countryCodePicker.fullNumberWithPlus
            it.priority.guestPriorityId = priority.selectedItemPosition
            it.accompanyings = accompanyings.text.toString()

            responsibleList?.let { listResponsible ->
                it.guestResponsible.id = listResponsible[responsible.selectedItemPosition].id
            }
        }
    }

    override fun showProgress(isVisible: Boolean) {
        progressLayout.visible = isVisible
    }
}
