package com.digitalhorizon.eve.ui.fragment.guestcard

import android.Manifest
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.common.ScreenPool
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.pawegio.kandroid.visible
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.fragment_about_guest.*

class AboutGuestFragment : MvpAppCompatFragment() {
    companion object {
        private const val DATA_KEY = "data_key"

        fun getInstance(guesCard: GuestCard?): AboutGuestFragment {
            val fragment = AboutGuestFragment()
            guesCard?.let {
                val arguments = Bundle()
                arguments.putSerializable(DATA_KEY, it)
                fragment.arguments = arguments
            }
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about_guest,container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            val guestCard = it.getSerializable(DATA_KEY) as GuestCard
            setGuestInfo(guestCard)
        }
    }

    private fun setGuestInfo(guestCard: GuestCard){
        companyName.text = guestCard.company?.companyName
        position.text = guestCard.position
        phone.text = guestCard.phone
        email.text = guestCard.email
        accompanying.text = guestCard.accompanyings
        iconPhone.visible = !guestCard.phone.isNullOrEmpty()

        iconPhone.setOnClickListener {
            RxPermissions(this.requireActivity())
                    .request(Manifest.permission.CALL_PHONE)
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe { isGranted ->
                        if (isGranted) {
                            ApplicationWrapper.INSTANCE.getRouter().navigateTo(ScreenPool.CALL_PHONE_ACTIVITY, guestCard.phone)
                        }
                    }
        }
    }
}