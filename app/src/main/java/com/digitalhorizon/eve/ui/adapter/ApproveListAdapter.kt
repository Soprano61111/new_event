package com.digitalhorizon.eve.ui.adapter

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.common.ScreenPool
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.presenter.ApproveListPresenter
import com.digitalhorizon.eve.utils.ImageUtils
import com.squareup.picasso.Picasso
import com.tr4android.recyclerviewslideitem.SwipeAdapter
import com.tr4android.recyclerviewslideitem.SwipeConfiguration
import kotlinx.android.synthetic.main.list_item_approve.view.*

class ApproveListAdapter(val context: Context?, val presenter: ApproveListPresenter) : SwipeAdapter() {
    var dataset = ArrayList<GuestCard>()

    override fun onCreateSwipeViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.list_item_approve, parent, true)
        return ApproveListViewHolder(view)
    }

    override fun onCreateSwipeConfiguration(context: Context, position: Int): SwipeConfiguration? {
        val builder = SwipeConfiguration.Builder(context)
                .setLeftBackgroundColorResource(R.color.roman)
                .setRightBackgroundColorResource(R.color.apple)
                .setLeftDrawableResource(R.drawable.ic_decline)
                .setRightDrawableResource(R.drawable.ic_check)
                .setUndoable(false)

        if (!dataset[position].guestCardOperations.isApproval) {
            builder.setLeftSwipeBehaviour(SwipeConfiguration.SwipeBehaviour.NO_SWIPE)
        }

        if (!dataset[position].guestCardOperations.isRefused) {
            builder.setRightSwipeBehaviour(SwipeConfiguration.SwipeBehaviour.NO_SWIPE)
        }

        return builder.build()
    }

    override fun onBindSwipeViewHolder(swipeViewHolder: RecyclerView.ViewHolder, position: Int) {
        val card = dataset[position]
        val approveListViewHolder = swipeViewHolder as ApproveListViewHolder
        approveListViewHolder.guestFullName.text = "${card.surname} ${card.firstName} ${card.lastName}"
        approveListViewHolder.ltdName.text = card.company?.companyName
        approveListViewHolder.postName.text = card.position
        approveListViewHolder.guestPriority.text = card.priority.name

        setImage(approveListViewHolder.guestAvatar, card)

        approveListViewHolder.rootLayout.setOnClickListener {
            ApplicationWrapper.INSTANCE.getRouter().navigateTo(ScreenPool.GUEST_CARD_FRAGMENT, dataset[position])
        }
    }

    override fun onSwipe(position: Int, direction: Int) {
        if (direction == SWIPE_LEFT) {
            showDialog(dataset[position])
        } else if (direction == SWIPE_RIGHT) {
            presenter.showApproveDialog(dataset[position])
        }
        notifyItemChanged(position)
    }

    override fun getItemCount() = dataset.size

    private fun setImage(cardImage: ImageView, card: GuestCard) {
        card.photo?.pictureName?.let {
            Picasso.with(cardImage.context)
                    .load(ImageUtils.getImagePath(it))
                    .centerCrop()
                    .resizeDimen(R.dimen.view_header_avatar_size, R.dimen.view_header_avatar_size)
                    .into(cardImage)
        } ?: run {
            context?.let {
                cardImage.setImageDrawable(ColorDrawable(ContextCompat.getColor(context, R.color.colorPrimaryLight)))
            }
        }
    }

    fun removeItem(guestCard: GuestCard) {
        val position = dataset.indexOf(guestCard)
        dataset.removeAt(position)
        notifyDataSetChanged()
    }

    class ApproveListViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val guestAvatar = item.guestAvatar
        val guestFullName = item.guestFullName
        val ltdName = item.ltdName
        val postName = item.postName
        val rootLayout = item.rootLayout
        val guestPriority = item.guestPriority
    }

    private fun showDialog(guestCardAvatar: GuestCard) {
        presenter.showRejectDialog(guestCardAvatar)
    }
}