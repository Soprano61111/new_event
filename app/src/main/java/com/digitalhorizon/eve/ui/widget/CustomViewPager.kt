package com.digitalhorizon.eve.ui.widget

import android.content.Context
import android.support.v4.view.ViewPager
import android.util.AttributeSet
import android.view.MotionEvent

class CustomViewPager(context: Context, attrs: AttributeSet) : ViewPager(context, attrs) {
    var enabled: Boolean? = true

    override fun onTouchEvent(event: MotionEvent): Boolean {
        return if (this.enabled!!) super.onTouchEvent(event) else false
    }

    override fun onInterceptTouchEvent(event: MotionEvent): Boolean {
        return if (this.enabled!!) super.onInterceptTouchEvent(event) else false
    }

    override fun canScrollHorizontally(direction: Int): Boolean {
        return if (this.enabled!!) super.canScrollHorizontally(direction) else false
    }

    fun setPagingEnabled(enabled: Boolean) {
        this.enabled = enabled
    }
}