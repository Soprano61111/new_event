package com.digitalhorizon.eve.ui.fragment

import android.os.Bundle
import android.os.CountDownTimer
import android.support.v4.content.ContextCompat
import android.view.View
import android.view.inputmethod.EditorInfo
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.FragmentWrapper
import com.digitalhorizon.eve.mvp.model.auth.SmsRegistration
import com.digitalhorizon.eve.mvp.presenter.RegistrationPresenter
import com.digitalhorizon.eve.mvp.view.RegistrationView
import com.digitalhorizon.eve.utils.TimerUtils
import com.digitalhorizon.eve.utils.fromHtml
import com.digitalhorizon.eve.utils.hideKeyboard
import com.pawegio.kandroid.textWatcher
import com.pawegio.kandroid.visible
import kotlinx.android.synthetic.main.fragment_registration_sms.*

class SMSConfirmFragment : FragmentWrapper(), RegistrationView {
    companion object {
        private const val DATA_KEY = "data"
        fun getInstance(smsRegistration: SmsRegistration?): SMSConfirmFragment {
            val fragment = SMSConfirmFragment()

            smsRegistration?.let {
                val argument = Bundle()
                argument.putSerializable(DATA_KEY, it)
                fragment.arguments = argument
            }
            return fragment
        }
    }

    @InjectPresenter
    lateinit var presenter: RegistrationPresenter

    private val MAX_CODE_LENGTH = 4
    private var countDownTimer: CountDownTimer? = null
    private var countDownTimerRetrySms: CountDownTimer? = null

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_registration_sms
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val smsRegistration = arguments?.getSerializable(DATA_KEY) as? SmsRegistration
        setHomeAsUpIndicator(TOOLBAR_INDICATOR_BACK_ARROW)
        setTitle(getString(R.string.back))

        sendAgain.text = fromHtml(getString(R.string.auth_send_again))
        inputSms.inputType = EditorInfo.TYPE_CLASS_NUMBER
        keyboard.attachEditText(inputSms)

        smsRegistration?.let { data ->
            inputSms.textWatcher {
                onTextChanged { charSequence, _, _, _ ->
                    charSequence?.let { str ->
                        showErrorMessage(false)

                        if (str.length == MAX_CODE_LENGTH) {
                            presenter.confirmSmsCode(data.regData, str.toString())
                        }
                    }
                }
            }

            sendAgain.setOnClickListener {
                presenter.sendAgainSmsCode(data.regData)
            }

            countDownRetrySms(data.countdownTimeout)
        }
    }

    override fun onDetach() {
        super.onDetach()
        activity?.hideKeyboard()
        countDownTimer?.cancel()
        countDownTimerRetrySms?.cancel()
    }

    /**
     * view state functions
     */

    override fun showProgress(isVisible: Boolean) {
        layoutProgress.visible = isVisible
    }

    override fun showCheckPhoneProgress(isVisible: Boolean) {}

    override fun showErrorMessage(isVisible: Boolean, msg: Int, countDown: Long, isBlocked: Boolean) {
        errorMsg.visible = isVisible

        context?.let {
            if (isVisible && !isBlocked) {
                inputSms.setTextColor(ContextCompat.getColor(it, R.color.monza))
            } else {
                inputSms.setTextColor(ContextCompat.getColor(it, R.color.colorPrimaryDark))
            }
        }

        if (isBlocked) {
            sendAgain?.visible = false
            keyboard.isKeyboardEnabled(false)

            if (countDownTimer != null) {
                countDownTimer?.cancel()
            }

            val listener = object : TimerUtils.OnTimerFinishListener {
                override fun onFinish() {
                    showErrorMessage(false)
                }
            }
            countDownTimer = TimerUtils().getTimer(context,
                    countDown,
                    1000,
                    msg,
                    listener,
                    errorMsg).start()
        } else {
            keyboard.isKeyboardEnabled(true)
            sendAgain?.visible = true
            countDownTimer?.cancel()
            errorMsg?.text = if (msg == -1) "" else getText(msg)
        }
    }

    override fun onCheckedPhone(isValid: Boolean) {}

    override fun showCountDownRetrySms(countDown: Long) {
        countDownRetrySms(countDown)
    }

    /**
     * other functions
     */

    private fun countDownRetrySms(countDown: Long) {
        sendAgain.isEnabled = false

        if (countDownTimerRetrySms != null) {
            countDownTimerRetrySms?.cancel()
        }

        val listener = object : TimerUtils.OnTimerFinishListener {
            override fun onFinish() {
                sendAgain.isEnabled = true
                sendAgain?.text = fromHtml(getString(R.string.auth_send_again))
            }
        }
        countDownTimerRetrySms = TimerUtils().getTimer(context,
                countDown,
                1000,
                R.string.auth_send_again_msg,
                listener,
                sendAgain).start()
    }
}