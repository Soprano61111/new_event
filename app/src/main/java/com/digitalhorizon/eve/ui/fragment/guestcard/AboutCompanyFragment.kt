package com.digitalhorizon.eve.ui.fragment.guestcard

import android.os.Bundle
import android.support.v7.widget.LinearLayoutManager
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.arellomobile.mvp.MvpAppCompatFragment
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.mvp.model.Company
import com.digitalhorizon.eve.ui.adapter.PhonesCompanyListAdapter
import com.pawegio.kandroid.visible
import kotlinx.android.synthetic.main.fragment_about_company.*

class AboutCompanyFragment : MvpAppCompatFragment() {
    companion object {
        private const val DATA_KEY = "data_key"

        fun getInstance(company: Company?): AboutCompanyFragment {
            val fragment = AboutCompanyFragment()
            company?.let {
                val arguments = Bundle()
                arguments.putSerializable(DATA_KEY, it)
                fragment.arguments = arguments
            }
            return fragment
        }
    }

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View? {
        return inflater.inflate(R.layout.fragment_about_company, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        arguments?.let {
            val company = it.getSerializable(DATA_KEY) as Company
            setCompanyInfo(company)
        }
    }

    private fun setCompanyInfo(company: Company){
        val adapter = PhonesCompanyListAdapter(this)
        company.phones?.let {
            adapter.dataset.addAll(it)
        }
        phoneList.adapter = adapter
        phoneList.isNestedScrollingEnabled = false
        phoneList.layoutManager = LinearLayoutManager(context)
        adapter.notifyDataSetChanged()

        companyName.text = company.companyName
        innNumber.text = company.inn
        emailLabel.visible = false
        email.visible = false
    }
}