package com.digitalhorizon.eve.ui.fragment

import android.os.Bundle
import android.view.View
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.AccountManager
import com.digitalhorizon.eve.common.FragmentWrapper
import com.digitalhorizon.eve.mvp.model.RoleEnum
import kotlinx.android.synthetic.main.fragment_stub_roles.*

//TODO: remove this fragment after implement all roles
class StubRolesFragment : FragmentWrapper() {

    companion object {
        fun getInstance() = StubRolesFragment()
    }

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_stub_roles
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setHomeAsUpIndicator(TOOLBAR_INDICATOR_HAMBURGER)
        showToolbar(TOOLBAR_HIDE)
        val notImpmented = "NOT IMPLEMENTED"

        when(AccountManager.role) {
            RoleEnum.ADMINISTRATOR -> text.text = "Administrator\n$notImpmented"
            RoleEnum.GUEST_MANAGER -> text.text = "Guest manager\n$notImpmented"
            RoleEnum.EVENT_MANAGER -> text.text = "Event manager\n$notImpmented"
            RoleEnum.GUEST_RESPONSIBLE -> text.text = "Guest responsible\n$notImpmented"
            RoleEnum.STRUCTURE_UNIT_OFFICER -> text.text = "Structure unit officer\n$notImpmented"
        }
    }
}