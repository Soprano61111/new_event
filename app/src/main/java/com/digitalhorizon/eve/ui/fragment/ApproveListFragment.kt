package com.digitalhorizon.eve.ui.fragment

import android.annotation.SuppressLint
import android.content.Context
import android.os.Bundle
import android.support.constraint.ConstraintSet
import android.support.v4.content.ContextCompat
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.StaggeredGridLayoutManager
import android.text.Editable
import android.text.TextWatcher
import android.transition.AutoTransition
import android.transition.TransitionManager
import android.view.Gravity
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.*
import com.digitalhorizon.eve.mvp.model.*
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardPriority
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardState
import com.digitalhorizon.eve.mvp.presenter.ApproveListPresenter
import com.digitalhorizon.eve.mvp.view.ApproveListView
import com.digitalhorizon.eve.ui.adapter.ApproveListAdapter
import com.digitalhorizon.eve.ui.adapter.FilterAdapterPriority
import com.digitalhorizon.eve.ui.adapter.FilterAdapterState
import com.digitalhorizon.eve.ui.fragment.guestcard.GuestCardForSUOFragment
import com.digitalhorizon.eve.ui.widget.ApproveDialog
import com.digitalhorizon.eve.ui.widget.DeclineDialog
import com.digitalhorizon.eve.ui.widget.CustomToast
import com.digitalhorizon.eve.utils.ImageUtils
import com.digitalhorizon.eve.utils.hideKeyboard
import com.digitalhorizon.eve.utils.showKeyboard
import com.paginate.Paginate
import com.pawegio.kandroid.runOnUiThread
import com.pawegio.kandroid.visible
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_approve_list.*
import kotlinx.android.synthetic.main.view_drawer_menu.*
import kotlinx.android.synthetic.main.view_toolbar_with_search_filter.*
import org.greenrobot.eventbus.EventBus
import org.greenrobot.eventbus.Subscribe
import org.greenrobot.eventbus.ThreadMode
import java.util.*
import kotlin.collections.ArrayList


class ApproveListFragment : FragmentWrapper(), ApproveListView, Paginate.Callbacks {
    companion object {
        fun getInstance() = ApproveListFragment()
    }

    @InjectPresenter
    lateinit var presenter: ApproveListPresenter

    /**
     * Watcher for Search bar
     */
    val watcher = object : TextWatcher {
        override fun afterTextChanged(p0: Editable?) {
           // clear.visible = query.textSize > 0
            scheduleQuery()
        }

        override fun beforeTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
        override fun onTextChanged(p0: CharSequence?, p1: Int, p2: Int, p3: Int) {}
    }

    private val router = ApplicationWrapper.INSTANCE.getRouter()
    private var adapter: ApproveListAdapter? = null
    private var getFiltersPriority = ArrayList<GuestCardPriority>()
    private var getFiltersState = ArrayList<GuestCardState>()
    private val DIALOG_REJECT = "dialog_reject"
    private val DIALOG_APPROVE = "dialog_approve"
    private var FILTER_LIST = "save_filters"
    private var FILTER_LIST_PRIORITY = "save_filters_priority"
    private var loading = false
    private var paginator: Paginate? = null
    private var queryTimer: Timer? = null
    private val SEARCH_DELAY: Long = 1000

    override fun getFragmentLayout(): Int {
        return R.layout.fragment_approve_list
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        presenter.getProfile()

//        AccountManager.priorities?.let {
//            getFiltersPriority = it
//        }

        showToolbar(TOOLBAR_EMBEDDED, R.layout.view_toolbar_with_search_filter)
        setHomeAsUpIndicator(TOOLBAR_INDICATOR_HAMBURGER, R.color.white)
        setTitleColor(R.color.white)
        setToolbarBackgroundDrawable(R.drawable.bg_vertical_gradient)
        initListItems()

        swipe.setOnRefreshListener {
            swipe.isRefreshing = false
            presenter.pullToRefresh()
        }

        router.setResultListener(GuestCardForSUOFragment.SEND_FOR_APPROVAL_RESULT) {
            onChangedGuestCardState(it as GuestCard, GuestCardState.SENDFORAPPROVAL)
        }

        router.setResultListener(GuestCardForSUOFragment.APPROVAL_RESULT) {
            onChangedGuestCardState(it as GuestCard, GuestCardState.AGREED)
        }

        router.setResultListener(GuestCardForSUOFragment.CHANGE_REQUEST_RESULT) {
            onChangedGuestCardState(it as GuestCard, GuestCardState.CHANGEREQUEST)
        }

        router.setResultListener(GuestCardForSUOFragment.DELETE_RESULT) {
            onChangedGuestCardState(it as GuestCard, GuestCardState.DELETED)
        }

        router.setResultListener(GuestCardForSUOFragment.REJECT_RESULT) {
            onChangedGuestCardState(it as GuestCard, GuestCardState.REFUSED)
        }

        //todo do not used ~ val toolbar = getToolbarView()

        val loadFilterListState = context?.let { Settings.LoadFilterListState(FILTER_LIST, it) }
        val loadFilterListPriority = context?.let { Settings.LoadFilterListPriority(FILTER_LIST_PRIORITY, it) }

        if (loadFilterListState?.size != 0) {
            getFiltersState = loadFilterListState!!
        }
        if (loadFilterListPriority?.size != 0) {
            getFiltersPriority = loadFilterListPriority!!
        }

        setFilters()
        setIndicatorView()
        prepareSearch()

        filter.setOnClickListener { openFilter() }
    }

    private fun prepareSearch() {
        //search block
        //listeners
        search.setOnClickListener {
            //show Search state
            searchView()

            //show keyboard
            activity?.let {
                showKeyboard()
            }
        }

        //clear text listener
//        clear.setOnClickListener {
//            clearQuery()
//        }

        //when bar closed we've to go to default state
//        cancel.setOnClickListener() {
//            if (query.text.isNotEmpty()) {
//                //get default list
//                presenter.search("")
//            }
//
//            clearQuery()
//            defaultView()
//        }
    }

    private fun cancelQuery(): Timer {
        //new fresh timer for our tasks...
        queryTimer?.cancel()
        queryTimer = Timer()

        return queryTimer!!
    }

    private fun scheduleQuery() {
        val timer = cancelQuery()

        //is there something so search?
        //prepare task
        val task = object : TimerTask() {
            override fun run() {
//                runOnUiThread {
//                    query?.text?.let {
//                        presenter.search(it.toString())
//                    }
//                }
            }
        }

        //go! go! go!
        timer.schedule(task, SEARCH_DELAY)
    }

    private fun clearQuery() {
//        if (query.text.trim().isNotEmpty()) {
//            presenter.search("")
//        }
//
//        query.text.clear()
//        clear.visible = false

        cancelQuery()
    }

    /**
     * Prepare views for search
     */
    private fun searchView() {
        //visibility thimblerig
        search.visibility = View.GONE
        toolbarIndicator.visibility = View.GONE
        countGuests.visibility = View.GONE
//        searchBar.visibility = View.VISIBLE
    }

    /**
     * Cancel search and restore default view
     */
    private fun defaultView() {
        //visibility thimblerig
        search.visibility = View.VISIBLE
        toolbarIndicator.visibility = View.VISIBLE
        countGuests.visibility = View.VISIBLE
//        searchBar.visibility = View.GONE

        //hide keyboard
        hideKeyboard()
    }

    override fun onPause() {
        super.onPause()
        //query text listener
//        query.removeTextChangedListener(watcher)
        hideKeyboard()
    }

    override fun onResume() {
        super.onResume()

        //restore search state when we're back
//        if (!query.text.isEmpty()) {
//            searchView()
//            showKeyboard()
//
//            clear.visible = query.textSize > 0
//        }
//
//        //query text listener
//        query.addTextChangedListener(watcher)
    }

    private fun showKeyboard() {
//        query.requestFocus()
        activity?.showKeyboard()
    }

    private fun hideKeyboard() {
        activity?.hideKeyboard()
    }

    private fun initListItems(isRestoreStateList: Boolean = true) {
        if (adapter == null || !isRestoreStateList) {
            adapter = ApproveListAdapter(activity, presenter)
        }

        val mNoOfColumns = context?.let { calculateNoOfColumns(it) }

        approveList.adapter = adapter
        filterButtonsPriority.adapter = FilterAdapterPriority(context, getFiltersPriority)
        filterButtonsState.adapter = FilterAdapterState(context)

        approveList.layoutManager = LinearLayoutManager(activity)
        filterButtonsPriority.layoutManager = mNoOfColumns?.let { StaggeredGridLayoutManager(it + 1, StaggeredGridLayoutManager.VERTICAL) }
        filterButtonsState.layoutManager = mNoOfColumns?.let { StaggeredGridLayoutManager(it + 1, StaggeredGridLayoutManager.VERTICAL) }

        if (!presenter.isLoadedAllItems() || !isRestoreStateList) {
            Paginate.with(approveList, this).build()
        }
    }

    private fun onChangedGuestCardState(guestCard: GuestCard, state: GuestCardState) {
        val toast = CustomToast.imgWithTextToast(activity, guestCard, state)
        toast.setGravity(Gravity.TOP, 0, 0)
        toast.show()

        adapter?.removeItem(guestCard)
    }

    override fun onLoadMore() {
        loading = true
        presenter.loadCards()
    }

    override fun isLoading() = loading

    override fun hasLoadedAllItems(): Boolean {
        if (presenter.isLoadedAllItems()) {
            paginator?.unbind()
            return true
        }
        return false
    }

    override fun onRejectGuestCard(guestCard: GuestCard) {
        onChangedGuestCardState(guestCard, GuestCardState.REFUSED)
    }

    override fun onApproveGuestCard(guestCard: GuestCard) {
        onChangedGuestCardState(guestCard, GuestCardState.AGREED)
    }

    override fun showToast(resId: Int) {
        Toast.makeText(activity, getText(resId), Toast.LENGTH_LONG).show()
    }

    override fun showRejectDialog(guestCard: GuestCard) {
        val myDialogFragment = DeclineDialog.getInstance(R.layout.dialog_decline_pressed, guestCard, R.string.dialog_title_decline,
                object : DeclineDialog.ReadyListener {
                    override fun ready(refused: String, guestCard: GuestCard) {
                        presenter.rejectGuestCard(guestCard, refused)
                    }
                })
        myDialogFragment.show(childFragmentManager, DIALOG_REJECT)
    }

    override fun showApproveDialog(guestCard: GuestCard) {
        val dialog = ApproveDialog.getInstance(guestCard)
        dialog.listener = object : ApproveDialog.OnSelectedListener {
            override fun onSelected(card: GuestCard, priority: GuestCardPriority) {
                presenter.approveGuestCard(card, priority)
            }
        }
        dialog.show(childFragmentManager, DIALOG_APPROVE)
    }

    fun calculateNoOfColumns(context: Context): Int {
        val displayMetrics = context.getResources().getDisplayMetrics()
        val dpWidth = displayMetrics.widthPixels / displayMetrics.density
        return (dpWidth / 180).toInt()
    }

    override fun checkGuestPriorities(card: GuestCard) {
//        AccountManager.priorities?.let {
//            showApproveDialog(card)
//        } ?: run {
//            presenter.getGuestPriorities(card)
//        }
    }

    override fun onLoadItems(items: ApproveList) {
        loading = false
        presenter.lastLoadedCount = items.items.size
        countGuests.text = String.format(getString(R.string.guest_count), items.count)
        presenter.itemsPage++
        adapter?.dataset?.addAll(items.items)
        adapter?.notifyDataSetChanged()
    }

    override fun onReload() {
        presenter.itemsPage = 0
        presenter.lastLoadedCount = 0
        loading = false

        initListItems(false)
    }

    @SuppressLint("SetTextI18n")
    override fun setSUOCard(card: StafferCard) {
        val avatarView = (activity as ActivityWrapper).mainAvatar
        val drawerMenuUserName = (activity as ActivityWrapper).mainUserName

        avatarView.visible = true
        drawerMenuUserName.visible = true

        drawerMenuUserName.text = "${card.surname}\n${card.name} ${card.additionalName}"

        card.photo?.pictureName?.let {
            Picasso.with(activity).load(ImageUtils.getImagePath(it)).into(avatarView)
        }
    }

    private fun openFilter() {
        filterLayout.visible = !filterLayout.visible
        val cs = ConstraintSet()
        val targetVisibility = if (filterLayout.visible) View.GONE else View.VISIBLE
        val colorId = if (filterLayout.visible) R.color.perano else R.color.white
        cs.clone(rootLayout)
        cs.setVisibility(R.id.filterLayout, targetVisibility)
        TransitionManager.beginDelayedTransition(rootLayout, AutoTransition().setDuration(200))
        cs.applyTo(rootLayout)
        context?.let { ContextCompat.getColor(it, colorId) }?.let { filter.setColorFilter(it) }

        if (!filterLayout.visible) {
            setFilters()
        }

        setIndicatorView()
        context?.let { Settings.SaveFilterListState(FILTER_LIST, getFiltersState, it) }
        context?.let { Settings.SaveFilterListPriority(FILTER_LIST_PRIORITY, getFiltersPriority, it) }
    }

    override fun onStart() {
        super.onStart()
        EventBus.getDefault().register(this)
    }

    override fun onStop() {
        super.onStop()
        EventBus.getDefault().unregister(this)
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun getMessage(event: Events.State) {
        getFiltersState = event.state
    }

    @Subscribe(threadMode = ThreadMode.MAIN)
    fun getMessage(event: Events.Priority) {
        getFiltersPriority = event.priority
    }

    fun setFilters() {
        val filterPriority = ArrayList<Int>()
        filterPriority.clear()
        presenter.setFilters(this.getFiltersState, getFiltersPriority)
    }

    fun setIndicatorView() {
        val colorIdIndicator = if (filterLayout.visible) R.color.polo_blue else R.color.california
        context?.let { ContextCompat.getColor(it, colorIdIndicator) }?.let { filterIndicator.setColorFilter(it) }

        indicatorsLayout.visibility = View.VISIBLE
        filterCount.text = (getFiltersState.size + getFiltersPriority.size).toString()
        if (getFiltersState.isEmpty() && getFiltersPriority.isEmpty()) {
            indicatorsLayout.visibility = View.GONE
        }
    }
}