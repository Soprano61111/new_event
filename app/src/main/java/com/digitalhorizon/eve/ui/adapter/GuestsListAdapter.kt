package com.digitalhorizon.eve.ui.adapter

import android.content.Context
import android.graphics.drawable.ColorDrawable
import android.support.v4.content.ContextCompat
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.common.FragmentWrapper
import com.digitalhorizon.eve.common.ScreenPool
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.presenter.GuestsListPresenter
import com.digitalhorizon.eve.ui.widget.CustomDialog
import com.digitalhorizon.eve.utils.ImageUtils
import com.squareup.picasso.Picasso
import com.tr4android.recyclerviewslideitem.SwipeAdapter
import com.tr4android.recyclerviewslideitem.SwipeConfiguration
import kotlinx.android.synthetic.main.list_item_guest.view.*

class GuestsListAdapter(val context: Context?, val presenter: GuestsListPresenter) : SwipeAdapter() {
    var dataset = ArrayList<GuestCard>()
    var dialog: CustomDialog? = null

    override fun onCreateSwipeViewHolder(parent: ViewGroup, i: Int): RecyclerView.ViewHolder {
        val view: View = LayoutInflater.from(parent.context).inflate(R.layout.list_item_guest, parent, true)
        return GuestsListViewHolder(view)
    }

    override fun onCreateSwipeConfiguration(context: Context, position: Int): SwipeConfiguration {
        val builder = SwipeConfiguration.Builder(context)
                .setUndoable(false)

        if (FragmentWrapper.isInvitedMode) {
            builder.setRightBackgroundColorResource(R.color.sapphire)
                    .setRightDrawableResource(R.drawable.ic_invite)
        } else {
            builder.setLeftBackgroundColorResource(R.color.roman)
                    .setRightBackgroundColorResource(R.color.apple)
                    .setLeftDrawableResource(R.drawable.ic_decline)
                    .setRightDrawableResource(R.drawable.ic_check)
        }

        if (!dataset[position].guestCardOperations.isApproval || FragmentWrapper.isInvitedMode) {
            builder.setLeftSwipeBehaviour(SwipeConfiguration.SwipeBehaviour.NO_SWIPE)
        }

        if (!dataset[position].guestCardOperations.isRefused) {
            builder.setRightSwipeBehaviour(SwipeConfiguration.SwipeBehaviour.NO_SWIPE)
        }

        return builder.build()
    }

    override fun getItemCount() = dataset.size

    override fun onBindSwipeViewHolder(swipeViewHolder: RecyclerView.ViewHolder, position: Int) {
        val card = dataset[position]
        val approveListViewHolder = swipeViewHolder as GuestsListViewHolder
        approveListViewHolder.guestFullName.text = "${card.surname} ${card.firstName} ${card.lastName}"
        approveListViewHolder.companyName.text = card.company?.companyName
        approveListViewHolder.positionName.text = card.position
        approveListViewHolder.priority.text = card.priority.name

        setImage(approveListViewHolder.avatar, card)

        approveListViewHolder.rootLayout.setOnClickListener {
            ApplicationWrapper.INSTANCE.getRouter().navigateTo(ScreenPool.GUEST_CARD_FRAGMENT, dataset[position])
        }
    }

    override fun onSwipe(position: Int, direction: Int) {
        if (!FragmentWrapper.isInvitedMode) {
            if (direction == SWIPE_LEFT) {
                presenter.showRejectDialog(dataset[position])
            } else if (direction == SWIPE_RIGHT) {
                presenter.showApproveDialog(dataset[position])
            }
        } else {
            if (direction == SWIPE_RIGHT) {
                showInviteDialog(dataset[position])
            }
        }
        notifyItemChanged(position)
    }

    private fun setImage(cardImage: ImageView, card: GuestCard) {
        card.photo?.pictureName?.let {
            Picasso.with(cardImage.context)
                    .load(ImageUtils.getImagePath(it))
                    .centerCrop()
                    .resizeDimen(R.dimen.view_header_avatar_size, R.dimen.view_header_avatar_size)
                    .into(cardImage)
        } ?: run {
            context?.let {
                cardImage.setImageDrawable(ColorDrawable(ContextCompat.getColor(context, R.color.colorPrimaryLight)))
            }
        }
    }

    fun removeItem(guestCard: GuestCard) {
        val position = dataset.indexOf(guestCard)
        dataset.removeAt(position)
        notifyDataSetChanged()
    }

    class GuestsListViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val avatar = item.avatar
        val guestFullName = item.guestFullName
        val companyName = item.companyName
        val positionName = item.positionName
        val rootLayout = item.rootLayout
        val priority = item.priority
    }

    private fun showInviteDialog(guestCard: GuestCard) {
        context?.let {
            dialog = CustomDialog(it).setTitle(R.string.dialog_invite_send_an_invitation_letter)
                    .setPositiveClickListeners(object : CustomDialog.OnSelectedPositiveListener {
                        override fun onSelectedItem() {
                            //TODO add send invited guest
                        }
                    })
                    .setNegativeClickListeners(object : CustomDialog.OnSelectedNegativeListener {
                        override fun onSelectedItem() {
                            dialog?.dismiss()
                        }
                    })
            dialog?.show()
        }
    }
}