package com.digitalhorizon.eve.ui.fragment.guestcard

import android.os.Bundle
import android.view.View
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.common.ScreenPool
import com.digitalhorizon.eve.mvp.model.Description
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardPriority
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardState
import com.digitalhorizon.eve.mvp.presenter.guestcard.GuestCardForSUOPresenter
import com.digitalhorizon.eve.mvp.view.guestcard.GuestCardForSUOView
import com.digitalhorizon.eve.ui.adapter.FragmentPagerAdapter
import com.digitalhorizon.eve.ui.widget.ApproveDialog
import com.digitalhorizon.eve.ui.widget.DeclineDialog
import com.digitalhorizon.eve.ui.widget.IOSSheetDialog
import com.digitalhorizon.eve.utils.ImageUtils
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.fragment_guest_card_for_suo.*
import kotlinx.android.synthetic.main.view_guest_card_head.*

class GuestCardForSUOFragment : BaseGuestCardFragment(), GuestCardForSUOView {
    companion object {
        const val REJECT_RESULT = 0
        const val SEND_FOR_APPROVAL_RESULT = 1
        const val APPROVAL_RESULT = 2
        const val DELETE_RESULT = 3
        const val CHANGE_REQUEST_RESULT = 4

        private const val DATA_KEY = "data_key"

        fun getInstance(guestCard: GuestCard?): GuestCardForSUOFragment {
            val fragment = GuestCardForSUOFragment()
            guestCard?.let {
                val arguments = Bundle()
                arguments.putSerializable(DATA_KEY, guestCard)
                fragment.arguments = arguments
            }
            return fragment
        }
    }

    @InjectPresenter
    lateinit var presenter: GuestCardForSUOPresenter

    private val ABOUT_GUEST_TAB = 0
    private val ABOUT_COMPANY_TAB = 1
    private val COMMENT_TAB = 2
    private val DIALOG_REJECT_TAG = "dialog_reject"
    private val DIALOG_SEND_FOR_APPROVAL_TAG = "dialog_send_for_approval"
    private val DIALOG_APPROVE_TAG = "dialog_approve"

    private val router = ApplicationWrapper.INSTANCE.getRouter()

    override fun getFragmentLayout() = R.layout.fragment_guest_card_for_suo

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        val guestCard = arguments?.getSerializable(DATA_KEY) as? GuestCard
        showToolbar(TOOLBAR_EMBEDDED)
        setToolbarBackgroundDrawable(R.drawable.bg_vertical_gradient)
        setHomeAsUpIndicator(TOOLBAR_INDICATOR_BACK_ARROW, R.color.white)
        setTitleColor(R.color.white)

        guestCard?.let { card ->
            val operations = card.guestCardOperations
            showGuestCard(guestCard)
            btnApprove.setOnClickListener { showApproveDialog(card) }
            btnReject.setOnClickListener { showRejectDialog(card) }
            btnMore.setOnClickListener { showDialogMore(card) }
            btnMore.isEnabled = (operations.isEscalate || operations.isChangeRequest || operations.isEdit || operations.isDelete)
            btnReject.isEnabled = operations.isRefused
            btnApprove.isEnabled = operations.isApproval
        }
    }

    override fun showGuestCard(guestCard: GuestCard) {
        val pagerAdapter = FragmentPagerAdapter(childFragmentManager)
        setTitle("${guestCard.surname} ${guestCard.firstName} ${guestCard.lastName}")
        setState(guestCard.state)
        guestPriority.text = guestCard.priority.name

        guestFullName.text = getString(
                R.string.guest_fullname_blank,
                guestCard.surname,
                guestCard.firstName,
                guestCard.lastName
        )

        pagerAdapter.fragmentList.addAll(arrayListOf(
                AboutGuestFragment.getInstance(guestCard),
                AboutCompanyFragment.getInstance(guestCard.company),
                CommentFragment.getInstance()
        ))

        viewPager.adapter = pagerAdapter
        pagerAdapter.notifyDataSetChanged()
        tabLayout.setupWithViewPager(viewPager)
        tabLayout.getTabAt(ABOUT_GUEST_TAB)?.text = getText(R.string.guest_tab_about_guest)
        tabLayout.getTabAt(ABOUT_COMPANY_TAB)?.text = getText(R.string.guest_tab_about_company)
        tabLayout.getTabAt(COMMENT_TAB)?.text = getText(R.string.comment)

        guestCard.photo?.pictureName?.let {
            Picasso.with(context)
                    .load(ImageUtils.getImagePath(it))
                    .resize(guestCard.photo!!.width, guestCard.photo!!.height)
                    .into(guestPhoto)
        }
    }

    private fun setState(state: GuestCardState) {
        guestState.text = getText(state.strResId)

        when (state) {
            GuestCardState.AGREED -> guestStateIndicator.setImageResource(R.drawable.ic_approved_state)
            GuestCardState.REFUSED -> guestStateIndicator.setImageResource(R.drawable.ic_wrong)
            GuestCardState.SENDFORAPPROVAL -> guestStateIndicator.setImageResource(R.drawable.ic_red_dot)
            GuestCardState.SAVED -> guestStateIndicator.setImageResource(R.drawable.ic_saved_state)
            else -> {  }
        }
    }

    private fun showDialogMore(guestCard: GuestCard) {
        context?.let {
            val dialog = IOSSheetDialog(it)
                    .setTitle(R.string.dialog_chose_action)
                    .setOnSelectedListener(object: IOSSheetDialog.OnSelectedItemListener{
                        override fun onSelectedItem(btnName: String) {
                            when(btnName){
                                getString(R.string.escalate) -> {
                                    presenter.sendForApprovalGuestCard(guestCard)
                                }
                                getString(R.string.change_request) -> {
                                    showChangeRequestDialog(guestCard)
                                }
                                getString(R.string.edit) -> {
                                    router.navigateTo(ScreenPool.EDIT_GUEST_CARD_FRAGMENT, guestCard)
                                }
                                getString(R.string.remove) -> {
                                    presenter.deleteGuestCard(guestCard)
                                }
                            }
                        }
                    })

            if (guestCard.guestCardOperations.isEscalate){
                dialog.addButton(R.string.escalate)
            }

            if (guestCard.guestCardOperations.isChangeRequest){
                dialog.addButton(R.string.change_request)
            }

            if (guestCard.guestCardOperations.isEdit){
                dialog.addButton(R.string.edit)
            }

            if (guestCard.guestCardOperations.isDelete){
                dialog.addButton(R.string.remove, R.color.red_orange)
            }

            dialog.show()
        }
    }

    private fun showRejectDialog(guestCard: GuestCard) {
        val dialog = DeclineDialog.getInstance(R.layout.dialog_decline_pressed, guestCard, R.string.dialog_title_decline, object : DeclineDialog.ReadyListener {
            override fun ready(refused: String, guestCard: GuestCard) {
                presenter.rejectGuestCard(guestCard, Description(refused))
            }
        })
        dialog.show(childFragmentManager, DIALOG_REJECT_TAG)
    }

    private fun showChangeRequestDialog(guestCard: GuestCard) {
        val dialog = DeclineDialog.getInstance(R.layout.dialog_decline_pressed, guestCard, R.string.dialog_title_return_for_revision, object : DeclineDialog.ReadyListener {
            override fun ready(refused: String, guestCard: GuestCard) {
                presenter.changeRequestGuestCard(guestCard, Description(refused))
            }
        })
        dialog.show(childFragmentManager, DIALOG_SEND_FOR_APPROVAL_TAG)
    }

    private fun showApproveDialog(guestCard: GuestCard) {
        val dialog = ApproveDialog.getInstance(guestCard)
        dialog.listener = object : ApproveDialog.OnSelectedListener {
            override fun onSelected(card: GuestCard, priority: GuestCardPriority) {
                presenter.approveGuestCard(guestCard, priority)
            }
        }
        dialog.show(childFragmentManager, DIALOG_APPROVE_TAG)
    }

    /**
     * view state functions
     */

    override fun showErrorMessage(msg: Int) {
        Toast.makeText(context, msg, Toast.LENGTH_LONG).show()
    }

    override fun showProgress(isVisible: Boolean) {
        loadingDialog(isVisible)
    }

    override fun onRejectGuestCard(guestCard: GuestCard) {
        router.exitWithResult(REJECT_RESULT, guestCard)
    }

    override fun onApproveGuestCard(guestCard: GuestCard) {
        router.exitWithResult(APPROVAL_RESULT, guestCard)
    }

    override fun onDeleteGuestCard(guestCard: GuestCard) {
        router.exitWithResult(DELETE_RESULT, guestCard)
    }

    override fun onSendForApprovalGuestCard(guestCard: GuestCard) {
        router.exitWithResult(SEND_FOR_APPROVAL_RESULT, guestCard)
    }

    override fun onChangeRequestGuestCard(guestCard: GuestCard) {
        router.exitWithResult(CHANGE_REQUEST_RESULT, guestCard)
    }
}