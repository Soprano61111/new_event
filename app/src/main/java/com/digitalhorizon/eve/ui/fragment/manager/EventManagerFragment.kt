package com.digitalhorizon.eve.ui.fragment.manager

import android.content.Context
import android.os.Bundle
import android.support.v4.app.Fragment
import android.view.View
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.common.ActivityWrapper
import com.digitalhorizon.eve.common.FragmentWrapper
import com.digitalhorizon.eve.mvp.presenter.manager.EventManagerPresenter
import com.digitalhorizon.eve.mvp.view.manager.EventManagerView
import com.digitalhorizon.eve.ui.fragment.GuestsListFragment
import com.pawegio.kandroid.visible
import kotlinx.android.synthetic.main.fragment_base_guest_manager.*
import kotlinx.android.synthetic.main.view_guests_items.*

class EventManagerFragment : BaseManagerFragment(), EventManagerView {
    companion object {
        fun getInstance() = EventManagerFragment()
    }

    @InjectPresenter
    lateinit var presenter: EventManagerPresenter

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        hamburgerClickListeners()
    }

    override fun onAttach(context: Context?) {
        super.onAttach(context)
        setHamburgerForRole()
    }

    override fun getFragmentList(): ArrayList<Fragment> {
        return arrayListOf(GuestsListFragment.getInstance())
    }

    private fun hamburgerClickListeners() {
        val fragmentPagerAdapter = viewPager.adapter
        (activity as ActivityWrapper).navItemApprove.setOnClickListener {
            FragmentWrapper.isInvitedMode = false
            fragmentPagerAdapter?.notifyDataSetChanged()
        }
        (activity as ActivityWrapper).navItemInvited.setOnClickListener {
            FragmentWrapper.isInvitedMode = true
            fragmentPagerAdapter?.notifyDataSetChanged()
        }
    }

    override fun prepareTabLayout() {
        tabLayout.visible = false
    }
}