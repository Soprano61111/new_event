package com.digitalhorizon.eve.ui.widget

import android.app.Dialog
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.view.WindowManager
import com.digitalhorizon.eve.R
import android.widget.Button
import android.widget.EditText
import android.widget.ImageView
import com.digitalhorizon.eve.utils.ImageUtils
import com.jakewharton.rxbinding2.widget.RxTextView
import com.squareup.picasso.Picasso
import io.reactivex.android.schedulers.AndroidSchedulers
import java.io.Serializable
import android.view.Gravity
import android.widget.TextView
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard


class DeclineDialog() : DialogFragment() {
    var ready: String = ""

    companion object {
        private const val LAYOUT = "layout"
        private const val GUEST_CARD = "guest_card"
        private const val READY_LISTENER = "ready_listener"
        private const val TITLE = "title"

        fun getInstance(layout: Int, guestCard: GuestCard, titleResId: Int, readyListener: ReadyListener?): DeclineDialog {
            val fragment = DeclineDialog()
            val arguments = Bundle()
            arguments.putInt(LAYOUT, layout)
            arguments.putSerializable(GUEST_CARD, guestCard)
            arguments.putSerializable(READY_LISTENER, readyListener)
            arguments.putInt(TITLE, titleResId)

            fragment.arguments = arguments
            return fragment
        }
    }

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val layout = arguments?.getInt(DeclineDialog.LAYOUT)
        val guestCard = arguments?.getSerializable(DeclineDialog.GUEST_CARD) as? GuestCard
        val readyListener = arguments?.getSerializable(DeclineDialog.READY_LISTENER) as? ReadyListener
        val titleResId = arguments?.getInt(DeclineDialog.TITLE)

        val dialog = Dialog(context)
        layout?.let {
            dialog.setContentView(it)
        }
        val lp = WindowManager.LayoutParams()
        lp.copyFrom(dialog.window.attributes)
        lp.width = WindowManager.LayoutParams.MATCH_PARENT
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT
        lp.gravity = Gravity.CENTER
        dialog.window.attributes = lp
        dialog.window?.setBackgroundDrawable(ColorDrawable(android.graphics.Color.TRANSPARENT))

        when (layout) {
            R.layout.dialog_decline_pressed -> {
                val save = dialog.findViewById<Button>(R.id.saveDialogButton)
                val foldOfSendText = dialog.findViewById<EditText>(R.id.foldOfSendText)
                val mainAvatar = dialog.findViewById<ImageView>(R.id.mainAvatar)
                val tvTitle = dialog.findViewById<TextView>(R.id.tvTitle)

                titleResId?.let {
                    tvTitle.text = getText(it)
                }

                foldOfSendText?.let {
                    RxTextView.afterTextChangeEvents(it)
                            .observeOn(AndroidSchedulers.mainThread())
                            .subscribe { tvChangeEvent ->
                                if (foldOfSendText.text.count() == 0) {
                                    save?.isEnabled = false
                                } else {
                                    save?.isEnabled = true
                                    ready = foldOfSendText.text.toString()
                                }
                            }
                }
                Picasso.with(mainAvatar?.context)
                        .load(guestCard?.photo?.pictureName?.let { ImageUtils.getImagePath(it) })
                        .centerCrop()
                        .resizeDimen(R.dimen.view_header_avatar_size, R.dimen.view_header_avatar_size)
                        .into(mainAvatar)

                save?.setOnClickListener {
                    guestCard?.let { listener -> readyListener?.ready(ready, listener) }
                    dialog.dismiss()
                }
            }
            //TODO else dialog    R.layout.dialog_decline_pressed -> doSomething()
            //TODO      else -> doSomething()
        }
        return dialog
    }

    interface ReadyListener : Serializable {
        fun ready(refused: String, guestCard: GuestCard)
    }
}