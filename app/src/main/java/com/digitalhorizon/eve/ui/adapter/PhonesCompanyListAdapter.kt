package com.digitalhorizon.eve.ui.adapter

import android.Manifest
import android.support.v4.app.Fragment
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.common.ScreenPool
import com.digitalhorizon.eve.mvp.model.PhonesCompany
import com.tbruyelle.rxpermissions2.RxPermissions
import io.reactivex.android.schedulers.AndroidSchedulers
import kotlinx.android.synthetic.main.list_item_phone.view.*

class PhonesCompanyListAdapter(var fragment: Fragment) : RecyclerView.Adapter<PhonesCompanyListAdapter.PhoneListViewHolder>() {
    val dataset: ArrayList<PhonesCompany> = ArrayList()

    override fun getItemCount() = dataset.size

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): PhoneListViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_phone, parent, false)
        return PhoneListViewHolder(view)
    }

    override fun onBindViewHolder(holder: PhoneListViewHolder, position: Int) {
        val item = dataset[position]

        holder.phone.text = item.phoneNumber
        holder.iconCall.setOnClickListener {
            fragment.activity?.let { activity ->
                RxPermissions(activity)
                        .request(Manifest.permission.CALL_PHONE)
                        .observeOn(AndroidSchedulers.mainThread())
                        .subscribe { isGranted ->
                            if (isGranted) {
                                ApplicationWrapper.INSTANCE.getRouter().navigateTo(ScreenPool.CALL_PHONE_ACTIVITY, item.phoneNumber)
                            }
                        }
            }
        }
    }

    class PhoneListViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val root = item.rootView
        val phone = item.phone
        val iconCall = item.iconPhone
    }
}