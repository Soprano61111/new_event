package com.digitalhorizon.eve.ui.widget

import android.app.Dialog
import android.os.Bundle
import android.support.v4.app.DialogFragment
import android.support.v7.app.AlertDialog
import android.view.LayoutInflater
import android.widget.Button
import android.widget.ToggleButton
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.AccountManager
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardPriority
import com.digitalhorizon.eve.utils.ImageUtils
import com.pawegio.kandroid.d
import com.squareup.picasso.Picasso
import kotlinx.android.synthetic.main.dialog_approve.view.*

class ApproveDialog : DialogFragment() {

    companion object {
        private const val DATA_KEY = "data_key"

        fun getInstance(guestCard: GuestCard?): ApproveDialog {
            val fragment = ApproveDialog()
            guestCard?.let {
                val arguments = Bundle()
                arguments.putSerializable(DATA_KEY, guestCard)
                fragment.arguments = arguments
            }
            return fragment
        }
    }

    private var priority: GuestCardPriority? = null
    private var buttons = ArrayList<ToggleButton>()
    private var save: Button? = null
    var listener: OnSelectedListener? = null
    private var priorities: ArrayList<GuestCardPriority> = arrayListOf()

    override fun onCreateDialog(savedInstanceState: Bundle?): Dialog {
        val myView = LayoutInflater.from(activity).inflate(R.layout.dialog_approve, null)
        val linear = myView.layoutButtons
        val card = arguments?.getSerializable(DATA_KEY) as? GuestCard

        priority = GuestCardPriority()
        save = myView.saveBtn

        save?.isEnabled = false

        AccountManager.getPriorities()
                .subscribe({ priorities ->
                    this.priorities.addAll(priorities)

                    for (i in 0 until priorities.size) {
                        val but = ToggleButton(context)
                        buttons.add(but)
                        buttons[i].text = priorities[i].name
                        buttons[i].transformationMethod = null
                        buttons[i].setBackgroundResource(R.drawable.toggle_btn_with_top_line_concrete)
                        buttons[i].stateListAnimator = null
                        buttons[i].textOn = priorities[i].name
                        buttons[i].textOff = priorities[i].name
                        buttons[i].setOnClickListener { view ->
                            val isChecked = (view as ToggleButton).isChecked
                            checkButtonPressed(isChecked, i)
                        }
                        linear.addView(buttons[i])
                    }
                }, {
                    d("Get Guest priorities error")
                })

        card?.photo?.pictureName?.let {
            Picasso.with(activity).load(ImageUtils.getImagePath(it)).into(myView.avatar)
        }

        save?.setOnClickListener {
            card?.let {
                onSelected(it, priority!!)
            }
        }

        return AlertDialog.Builder(activity!!, R.style.AlertDialog)
                .setView(myView)
                .create()
    }

    private fun checkButtonPressed(isChecked: Boolean, position: Int) {
        for (i in 0 until priorities.size) {
            if (i != position) {
                buttons[i].isChecked = false
                priority = priorities[position]
            }
        }
        save?.isEnabled = isChecked
    }

    private fun onSelected(card: GuestCard, priority: GuestCardPriority) {
        listener?.onSelected(card, priority)
        dismiss()
    }

    interface OnSelectedListener {
        fun onSelected(card: GuestCard, priority: GuestCardPriority)
    }
}