package com.digitalhorizon.eve.ui.fragment

interface FragmentLifeCycle {
    fun fragmentOnResume()
    fun fragmentOnPause()
}