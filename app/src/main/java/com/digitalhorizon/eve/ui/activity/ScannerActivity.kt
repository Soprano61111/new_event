package com.digitalhorizon.eve.ui.activity

import android.content.Context
import android.content.Intent
import android.os.Bundle
import android.support.v4.app.Fragment
import android.support.v4.app.FragmentTransaction
import android.view.WindowManager
import android.widget.Toast
import com.arellomobile.mvp.presenter.InjectPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.ActivityWrapper
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.common.ScreenPool
import com.digitalhorizon.eve.mvp.presenter.ScannerPresenter
import com.digitalhorizon.eve.mvp.view.ScannerView
import com.google.zxing.ResultPoint
import com.google.zxing.client.android.BeepManager;
import com.journeyapps.barcodescanner.BarcodeCallback
import com.journeyapps.barcodescanner.BarcodeResult
import com.journeyapps.barcodescanner.CaptureManager
import kotlinx.android.synthetic.main.activity_scanner.*
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.android.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command

class ScannerActivity : ActivityWrapper(), ScannerView, BarcodeCallback {
    companion object {
        const val SCANNER_REQUEST_CODE = 100
        fun getIntent(context: Context) = Intent(context, ScannerActivity::class.java)
    }

    @InjectPresenter
    lateinit var presenter: ScannerPresenter
    private lateinit var captureManage: CaptureManager
    private var beepManager: BeepManager? = null

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_scanner)
        window.setFlags(WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS, WindowManager.LayoutParams.FLAG_LAYOUT_NO_LIMITS)
        showToolbar(false)
        captureManage = CaptureManager(this, barcodeScanner)
        beepManager = BeepManager(this)
        barcodeScanner.decodeSingle(this)
        close.setOnClickListener {
            router.exit()
        }
    }

    override fun onResume() {
        super.onResume()
        ApplicationWrapper.INSTANCE.getNavigationHolder().setNavigator(navigator)
        captureManage.onResume()
    }

    override fun onPause() {
        super.onPause()
        ApplicationWrapper.INSTANCE.getNavigationHolder().removeNavigator()
        captureManage.onPause()
    }

    override fun barcodeResult(result: BarcodeResult?) {
        result?.let {
            beepManager?.playBeepSoundAndVibrate()
            router.exitWithResult(SCANNER_REQUEST_CODE, it.text)
        }
    }

    override fun possibleResultPoints(resultPoints: MutableList<ResultPoint>?) {

    }

    private val navigator: Navigator = object: SupportAppNavigator(this, R.id.mainContainer){
        override fun exit() {
            finish()
        }

        override fun createActivityIntent(context: Context, screenKey: String, data: Any?): Intent? {
            return ScreenPool.getActivity(context, screenKey, data)
        }

        override fun createFragment(screenKey: String, data: Any?): Fragment? {
            return ScreenPool.getFragment(screenKey, data)
        }

        override fun showSystemMessage(message: String?) {
            Toast.makeText(this@ScannerActivity, message, Toast.LENGTH_LONG).show()
        }

        override fun setupFragmentTransactionAnimation(command: Command?, currentFragment: Fragment?, nextFragment: Fragment?, fragmentTransaction: FragmentTransaction?) {

        }
    }
}