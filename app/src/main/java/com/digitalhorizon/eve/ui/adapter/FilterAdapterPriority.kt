package com.digitalhorizon.eve.ui.adapter

import android.content.Context
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.common.Settings
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardPriority
import kotlinx.android.synthetic.main.list_item_filter.view.*
import com.digitalhorizon.eve.mvp.model.Events
import org.greenrobot.eventbus.EventBus
import kotlin.collections.ArrayList


class FilterAdapterPriority(val context: Context?, val filterPriority:  ArrayList<GuestCardPriority>) : RecyclerView.Adapter<FilterAdapterPriority.FilterViewHolder>() {

    val mapPriority = LinkedHashMap<GuestCardPriority, Boolean>()
    var arrayGuestCardPriorityName = ArrayList<GuestCardPriority>()
    val arrayGuestCardPriorityBoolean = ArrayList<Boolean>()
    val finalGustCardPriority = ArrayList<GuestCardPriority>()
    private val SAVE_FILTER_MAP = "save_filter_map"

    init {
        //get file
        val newmap = context?.let { Settings.LoadLinkedHashMapFromInternalStoragePriority(SAVE_FILTER_MAP, it) }
        if (newmap?.size != 0) {

            mapPriority.clear()
            newmap?.let { mapPriority.putAll(it) }

        } else {
            //set card
            arrayGuestCardPriorityName = filterPriority
            if (mapPriority.isEmpty()) {
                for (i in 0 until arrayGuestCardPriorityName.size) {
                    mapPriority[arrayGuestCardPriorityName[i]] = false
                }
            }
        }

        for (entry in mapPriority) {
            val key = entry.key
            val tab = entry.value

            arrayGuestCardPriorityName.add(key)
            arrayGuestCardPriorityBoolean.add(tab)
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): FilterViewHolder {
        val view = LayoutInflater.from(parent.context).inflate(R.layout.list_item_filter, parent, false)
        return FilterViewHolder(view)
    }

    override fun onBindViewHolder(holder: FilterViewHolder, position: Int) {
        holder.button.text = null
        holder.button.textOn = null
        holder.button.textOff = null
        holder.button.setSingleLine(true)
        holder.button.text = arrayGuestCardPriorityName.get(position).name
        if (arrayGuestCardPriorityBoolean.get(position)) {
            holder.button.setChecked(true)
        } else {
            holder.button.setChecked(false)
        }
        holder.button.setOnClickListener {
            finalGustCardPriority.clear()
            if (arrayGuestCardPriorityBoolean.get(position)) {
                arrayGuestCardPriorityBoolean[position] = false
                holder.button.setChecked(false)
            } else {
                arrayGuestCardPriorityBoolean[position] = true
                holder.button.setChecked(true)
            }
            for (i in 0 until arrayGuestCardPriorityBoolean.size) {
                if (arrayGuestCardPriorityBoolean.get(i)) {
                    arrayGuestCardPriorityName.get(i).let { it1 -> finalGustCardPriority.add(it1) }
                }
            }

            if (arrayGuestCardPriorityName != null) {
                for (i in 0 until arrayGuestCardPriorityName.size) {
                    arrayGuestCardPriorityName.get(position).let { it1 -> mapPriority.put(it1, arrayGuestCardPriorityBoolean[position]) }
                }
            }
            EventBus.getDefault().post(Events.Priority(finalGustCardPriority))
            context?.let { it1 -> Settings.SaveLinkedHashMapToInternalStoragePriority(SAVE_FILTER_MAP, mapPriority, it1) }
        }
    }

    override fun getItemCount(): Int {
        return mapPriority.size
    }

    class FilterViewHolder(item: View) : RecyclerView.ViewHolder(item) {
        val button = item.title
    }
}