package com.digitalhorizon.eve.ui.adapter

import android.support.v4.app.Fragment
import android.support.v4.app.FragmentManager
import android.support.v4.app.FragmentStatePagerAdapter
import android.support.v4.view.PagerAdapter

class FragmentPagerAdapter(fm: FragmentManager): FragmentStatePagerAdapter(fm) {
    private var baseId: Long = 0
    val fragmentList = ArrayList<Fragment>()

    override fun getCount() = fragmentList.size

    override fun getItem(position: Int) = fragmentList[position]

    override fun getItemPosition(o: Any): Int {
        return PagerAdapter.POSITION_NONE
    }

    fun getItemId(position: Int): Long {
        // give an ID different from position when position has been changed
        return baseId + position
    }

    /**
     * Notify that the position of a fragment has been changed.
     * Create a new ID for each position to force recreation of the fragment
     * @param n number of items which have been changed
     */
    fun notifyChangeInPosition(n: Int) {
        // shift the ID returned by getItemId outside the range of all previous fragments
        baseId += count + n
    }
}