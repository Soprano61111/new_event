package com.digitalhorizon.eve.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.api.Api
import com.digitalhorizon.eve.api.HttpStatusCode
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.common.ScreenPool
import com.digitalhorizon.eve.mvp.model.Pass
import com.digitalhorizon.eve.mvp.view.SecurityView
import com.digitalhorizon.eve.ui.fragment.guestcard.GuestCardForSecurityFragment
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import retrofit2.HttpException

@InjectViewState
class SecurityPresenter : MvpPresenter<SecurityView>() {
    val router = ApplicationWrapper.INSTANCE.getRouter()

    fun getGuestCardData(pass: Pass) {
        viewState.progressBar(true)
        Api.Users.getGuestCard(pass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { viewState.progressBar(false) }
                .subscribe({
                    router.navigateTo(ScreenPool.GUEST_CARD_FRAGMENT, GuestCardForSecurityFragment.getBundle(pass, it))
                }, { e ->
                    if ((e as? HttpException)?.code() == HttpStatusCode.BAD_REQUEST) {
                        viewState.showViewGuestNotFound()
                    } else {
                        e.printStackTrace()
                    }
                })
    }

    fun getProfile() {
        Api.Auth.getStafferCard()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ card ->
                    viewState.setSecurityCard(card)
                }, {
                    it.printStackTrace()
                })
    }
}
