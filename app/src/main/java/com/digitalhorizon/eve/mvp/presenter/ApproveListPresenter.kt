package com.digitalhorizon.eve.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.api.Api
import com.digitalhorizon.eve.common.AccountManager
import com.digitalhorizon.eve.mvp.model.*
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardPriority
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardState
import com.digitalhorizon.eve.mvp.view.ApproveListView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import kotlin.collections.ArrayList
import org.json.JSONArray

@InjectViewState
class ApproveListPresenter : MvpPresenter<ApproveListView>() {
    val ITEMS_PRE_PAGE = 10
    var itemsPage = 0
    var lastLoadedCount = 0
    var searchString = ""
    val filterState = ArrayList<String>()
    val filterPriority = ArrayList<Int>()

    fun loadCards() {

        val filterStateJSONArray = JSONArray(filterState)
        val filterPriorityJSONArray = JSONArray(filterPriority)

        Api.Users.getApproveCards(ITEMS_PRE_PAGE, itemsPage * ITEMS_PRE_PAGE, filterStateJSONArray, filterPriorityJSONArray, searchString)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ cards ->
                    viewState.onLoadItems(cards)
                }, {
                    viewState.showToast(R.string.error_general)
                })
    }

    fun isLoadedAllItems(): Boolean {
        if (lastLoadedCount == ITEMS_PRE_PAGE || itemsPage == 0) {
            return false
        }
        return true
    }

    fun setFilters(filterS: ArrayList<GuestCardState>, filterP: ArrayList<GuestCardPriority>) {
        filterState.clear()
        filterPriority.clear()

        if (filterP.isNotEmpty()) {
            for (i in 0 until filterP.size) {
                this.filterPriority.add(i, filterP[i].id)
            }
        }

        if (filterS.isNotEmpty()) {
            for (i in 0 until filterS.size) {
                this.filterState.add(i, filterS[i].toString())
            }
        }
        resetList()
    }

    fun search(searchStr: String) {
        searchString = searchStr
        resetList()
    }

    fun pullToRefresh() {
        resetList()
    }

    private fun resetList() {
        itemsPage = 0
        lastLoadedCount = 0
        viewState.onReload()
    }

    fun getGuestPriorities(card: GuestCard) {
//        Api.Users.getGuestCardsPriorities()
//                .observeOn(AndroidSchedulers.mainThread())
//                .subscribe({
//                    AccountManager.priorities = it
//                    viewState.showApproveDialog(card)
//                }, { e ->
//                    viewState.showToast(R.string.error_general)
//                })
    }

    fun showApproveDialog(guestCard: GuestCard) {
        viewState.showApproveDialog(guestCard)
    }

    fun showRejectDialog(guestCard: GuestCard) {
        viewState.showRejectDialog(guestCard)
    }

    fun rejectGuestCard(guestCard: GuestCard, msg: String) {
        Api.Users.rejectGuestCard(guestCard.id, guestCard.rowVersion, Description(msg))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.onRejectGuestCard(guestCard)
                }, { e ->
                    viewState.showToast(R.string.error_general)
                })
    }

    fun approveGuestCard(guestCard: GuestCard, guestCardPriority: GuestCardPriority) {
        Api.Users.approveGuestCard(guestCard.id, guestCard.rowVersion, guestCardPriority)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.onApproveGuestCard(guestCard)
                }, {
                    viewState.showToast(R.string.error_general)
                })
    }

    fun getProfile() {
        Api.Auth.getStafferCard()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ card ->
                    AccountManager.userCard = card
                    viewState.setSUOCard(card)
                }, {
                    it.printStackTrace()
                })
    }
}