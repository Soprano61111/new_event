package com.digitalhorizon.eve.mvp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ResponsibleList : Serializable {
    @SerializedName("count")
    var count: Int = 0

    @SerializedName("items")
    var items: ArrayList<GuestResponsible> = ArrayList()
}