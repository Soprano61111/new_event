package com.digitalhorizon.eve.mvp.presenter

import android.graphics.Bitmap
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.api.Api
import com.digitalhorizon.eve.mvp.view.TicketView
import com.google.zxing.BarcodeFormat
import com.google.zxing.EncodeHintType
import com.google.zxing.qrcode.QRCodeWriter
import com.google.zxing.qrcode.decoder.ErrorCorrectionLevel
import com.journeyapps.barcodescanner.BarcodeEncoder
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.util.*

@InjectViewState
class TicketPresenter : MvpPresenter<TicketView>() {
    fun getQrCode() {
        viewState.showLoadingDialog(true)
        Api.Users.getPass()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ pass ->
                    viewState.setQrCode(setGenerateQr(pass.pass))
                    getCardGuest()
                }, {
                    viewState.showLoadingDialog(false)
                })
    }

    fun getCardGuest() {
        Api.Users.getAccount()
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { viewState.showLoadingDialog(false) }
                .subscribe({ card ->
                    viewState.setGuestAccount(card)
                }, { e ->
                    e.printStackTrace()
                })
    }

    fun setGenerateQr(pass: String): Bitmap {
        val writer = QRCodeWriter()
        val barcodeEncoder = BarcodeEncoder()

        //QR generation parameters
        val hints = Hashtable<EncodeHintType, Any>()
        hints.put(EncodeHintType.CHARACTER_SET, "UTF-8")
        hints.put(EncodeHintType.ERROR_CORRECTION, ErrorCorrectionLevel.M)

        val matrix = writer.encode(pass, BarcodeFormat.QR_CODE, 750, 750, hints)
        val bitmap = barcodeEncoder.createBitmap(matrix)
        return bitmap
    }
}