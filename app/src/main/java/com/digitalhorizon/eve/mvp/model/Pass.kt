package com.digitalhorizon.eve.mvp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Pass(passStr: String) : Serializable {
    @SerializedName("pass")
    var pass: String = passStr
}