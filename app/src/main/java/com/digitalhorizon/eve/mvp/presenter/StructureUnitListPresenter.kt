package com.digitalhorizon.eve.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.api.Api
import com.digitalhorizon.eve.mvp.view.StructureUnitListView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@InjectViewState
class StructureUnitListPresenter : MvpPresenter<StructureUnitListView>() {
    val ITEMS_PRE_PAGE = 10
    var itemsPage = 0
    var lastLoadedCount = 0
    var orderField: String? = null
    var sortOrder: String? = null

    fun loadStructureUnits() {
        Api.Users.getStructureUnits(ITEMS_PRE_PAGE, itemsPage * ITEMS_PRE_PAGE, orderField, sortOrder)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ structureUnits ->
                    viewState.onLoadItems(structureUnits)
                }, {
                    viewState.showToast(R.string.error_general)
                })
    }

    fun isLoadedAllItems(): Boolean {
        if (lastLoadedCount == ITEMS_PRE_PAGE || itemsPage == 0) {
            return false
        }
        return true
    }

    private fun resetList() {
        itemsPage = 0
        lastLoadedCount = 0
        viewState.onReload()
    }

    fun pullToRefresh() {
        resetList()
    }
}