package com.digitalhorizon.eve.utils

import com.digitalhorizon.eve.mvp.model.error.Detail
import com.digitalhorizon.eve.mvp.model.error.EnumErrorType
import com.google.gson.annotations.SerializedName

/**
 * Created by bender on 28/06/2017.
 */
class ServerException {

    @SerializedName("type")
    var type: EnumErrorType = EnumErrorType.ERRORS

    @SerializedName("correlation_id")
    var correlationId: String = ""

    @SerializedName("message")
    var message: String = ""

    @SerializedName("client_message")
    var clientMessage: String = ""

    @SerializedName("source")
    var source: String = ""

    @SerializedName("extra_fields")
    val extraFields: Detail? = null
}