package com.digitalhorizon.eve.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.mvp.view.ScannerView

@InjectViewState
class ScannerPresenter : MvpPresenter<ScannerView>() {

}