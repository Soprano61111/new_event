package com.digitalhorizon.eve.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.api.Api
import com.digitalhorizon.eve.mvp.model.Description
import com.digitalhorizon.eve.mvp.model.PurposeEnum
import com.digitalhorizon.eve.mvp.model.StructureUnit
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardPriority
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardState
import com.digitalhorizon.eve.mvp.view.GuestsListView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@InjectViewState
class GuestsListPresenter : MvpPresenter<GuestsListView>() {
    val ITEMS_PRE_PAGE = 10
    var itemsPage = 0
    var lastLoadedCount = 0
    var stateFilters: ArrayList<GuestCardState>? = null
    var priorityFilters: ArrayList<Int>? = null
    var structureUnits: ArrayList<StructureUnit>? = null
    var assigned: Int? = null
    var purposeEnum: PurposeEnum? = null
    var query: String? = null
    //var invateStateFilters = ArrayList<InviteState>()

    var isFiltersChanged = false

    fun getGuestCards() {
        Api.Users.getGuestCardsList(
                ITEMS_PRE_PAGE,
                itemsPage * ITEMS_PRE_PAGE,
                assigned,
                purposeEnum,
                stateFilters,
                priorityFilters,
                structureUnits,
                query
        )
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ guestCards ->
                    viewState.onLoadItems(guestCards)
                }, {
                    viewState.showToast(R.string.error_general)
                })
    }

    fun showApproveDialog(guestCard: GuestCard) {
        viewState.showApproveDialog(guestCard)
    }

    fun showRejectDialog(guestCard: GuestCard) {
        viewState.showRejectDialog(guestCard)
    }

    fun rejectGuestCard(guestCard: GuestCard, msg: String) {
        Api.Users.rejectGuestCard(guestCard.id, guestCard.rowVersion, Description(msg))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.onRejectGuestCard(guestCard)
                }, {
                    viewState.showToast(R.string.error_general)
                })
    }

    fun approveGuestCard(guestCard: GuestCard, guestCardPriority: GuestCardPriority) {
        Api.Users.approveGuestCard(guestCard.id, guestCard.rowVersion, guestCardPriority)
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.onApproveGuestCard(guestCard)
                }, {
                    viewState.showToast(R.string.error_general)
                })
    }

    fun isLoadedAllItems(): Boolean {
        if (lastLoadedCount == ITEMS_PRE_PAGE || itemsPage == 0) {
            return false
        }
        return true
    }

    fun refresh() {
        resetList()
    }

    private fun resetList() {
        itemsPage = 0
        lastLoadedCount = 0
        viewState.onReload()
    }
}