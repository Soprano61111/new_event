package com.digitalhorizon.eve.mvp.model.error

import com.google.gson.annotations.SerializedName

 class Timeout {
	 @SerializedName("elapsed")
	 val elapsed: String? = null

	 @SerializedName("errors")
	 var reasons: ArrayList<String>? = null
 }
