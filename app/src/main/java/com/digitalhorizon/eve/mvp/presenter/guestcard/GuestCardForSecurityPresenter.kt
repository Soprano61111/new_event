package com.digitalhorizon.eve.mvp.presenter.guestcard

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.api.Api
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.Pass
import com.digitalhorizon.eve.mvp.view.guestcard.GuestCardForSecurityView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@InjectViewState
class GuestCardForSecurityPresenter : MvpPresenter<GuestCardForSecurityView>() {
    val router = ApplicationWrapper.INSTANCE.getRouter()

    fun getCountDeny(guestCard: GuestCard) {
        guestCard.id?.let {
            Api.Users.getDenyGuest(it)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({
                        it.denyCount?.let {viewState.setDenyCount(it)}
                    }, { e ->
                        viewState.showErrorMessage(R.string.error_general)
                    })
        }
    }

    fun passGuest(pass: Pass) {
        Api.Users.passGuest(pass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    router.exit()
                }, { e ->
                    viewState.showErrorMessage(R.string.error_general)
                })
    }

    fun denyGuest(pass: Pass) {
        Api.Users.denyGuest(pass)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    router.exit()
                }, { e ->
                    viewState.showErrorMessage(R.string.error_general)
                })
    }
}
