package com.digitalhorizon.eve.mvp.view

import com.arellomobile.mvp.MvpView
import com.digitalhorizon.eve.mvp.model.ResponsibleList

interface EditGuestCardView : MvpView {
    fun setResponsible(list: ResponsibleList?)
    fun showToast(resId: Int)
    fun getCardFields()
    fun showProgress(isVisible: Boolean)
}