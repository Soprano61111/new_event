package com.digitalhorizon.eve.mvp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class StafferCard : Serializable {
    @SerializedName("user_identity_id")
    var userIdentityId: Int = 0

    @SerializedName("user_id")
    var userId: Int = 0

    @SerializedName("login")
    var login: String = ""

    @SerializedName("surname")
    var surname: String? = null
        get() {
            if (field.isNullOrEmpty()) {
                return ""
            }
            return field
        }

    @SerializedName("name")
    var name: String? = null
        get() {
            if (field.isNullOrEmpty()) {
                return ""
            }
            return field
        }

    @SerializedName("additional_name")
    var additionalName: String? = null
        get() {
            if (field.isNullOrEmpty()) {
                return ""
            }
            return field
        }

    @SerializedName("photo")
    var photo: Photo? = Photo()

    @SerializedName("email")
    var email: String = ""

    @SerializedName("roles")
    var roles: ArrayList<String> = ArrayList()

    @SerializedName("permissions")
    var permissions: ArrayList<String> = ArrayList()

    @SerializedName("structure_unit_roles")
    var structureUnitRoles: ArrayList<StructureUnitRoles> = ArrayList()
}