package com.digitalhorizon.eve.mvp.view.guestcard

import com.arellomobile.mvp.MvpView

interface BaseGuestCardView : MvpView {
    fun showErrorMessage(msg: Int)
}