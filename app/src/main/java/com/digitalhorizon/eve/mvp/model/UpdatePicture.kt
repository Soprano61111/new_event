package com.digitalhorizon.eve.mvp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UpdatePicture : Serializable {
    @SerializedName("picture_id")
    var pictureId: Int? = null
}