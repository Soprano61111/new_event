package com.digitalhorizon.eve.mvp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable


class Description(str: String) : Serializable {
	@SerializedName("description")
	var description: String = str
}
