package com.digitalhorizon.eve.mvp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Phone(phone: String) : Serializable {
    @SerializedName("number_phone")
    var phoneNumber: String = phone
}