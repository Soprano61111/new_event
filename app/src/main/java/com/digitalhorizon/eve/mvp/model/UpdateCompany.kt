package com.digitalhorizon.eve.mvp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UpdateCompany : Serializable {
    @SerializedName("company_id")
    var companyId: Int = 0
}