package com.digitalhorizon.eve.mvp.model

import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class ApproveList : Serializable {
    @SerializedName("count")
    var count: Int = 0

    @SerializedName("items")
    var items: ArrayList<GuestCard> = ArrayList()
}