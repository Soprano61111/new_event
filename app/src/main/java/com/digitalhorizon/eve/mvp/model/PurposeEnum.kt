package com.digitalhorizon.eve.mvp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

enum class PurposeEnum : Serializable {
    @SerializedName("approve")
    APPROVE,
    @SerializedName("approve_interested")
    APPROVE_INTERESTED,
    @SerializedName("interested")
    INTERESTED,
    @SerializedName("responsible")
    RESPONSIBLE,
    @SerializedName("edit")
    EDIT
}