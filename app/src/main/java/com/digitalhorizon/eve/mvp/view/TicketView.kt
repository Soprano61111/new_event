package com.digitalhorizon.eve.mvp.view

import android.graphics.Bitmap
import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard

@StateStrategyType(SkipStrategy::class)
interface TicketView : MvpView {
    fun setQrCode(bitmap: Bitmap)
    fun setGuestAccount(card: GuestCard)
    fun showLoadingDialog(isVisible: Boolean)
}
