package com.digitalhorizon.eve.mvp.model

import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardPriority
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardState

class Events {
    // Event used to send message from fragment to activity.
    class State(val state: ArrayList<GuestCardState>)

    // Event used to send message from activity to fragment.
    class Priority(val priority: ArrayList<GuestCardPriority>)
}

