package com.digitalhorizon.eve.mvp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UpdateId : Serializable {
    @SerializedName("id")
    var id: Int = 0
}