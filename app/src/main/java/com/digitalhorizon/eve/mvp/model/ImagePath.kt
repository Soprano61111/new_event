package com.digitalhorizon.eve.mvp.model

import com.google.gson.annotations.SerializedName

class ImagePath {
    @SerializedName("path")
    var imagePath: String = ""
}