package com.digitalhorizon.eve.mvp.view.manager

import com.arellomobile.mvp.MvpView
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard

interface ManagerView : MvpView {
    fun onLoadListItems(items: ArrayList<GuestCard>)
    fun onRefreshList()
    fun onError()
}