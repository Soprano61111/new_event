package com.digitalhorizon.eve.mvp.presenter.guestcard

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.api.Api
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardPriority
import com.digitalhorizon.eve.mvp.model.Description
import com.digitalhorizon.eve.mvp.view.guestcard.GuestCardForSUOView
import io.reactivex.android.schedulers.AndroidSchedulers

@InjectViewState
class GuestCardForSUOPresenter : MvpPresenter<GuestCardForSUOView>() {
    fun approveGuestCard(guestCard: GuestCard, guestCardPriority: GuestCardPriority) {
        viewState.showProgress(true)

        Api.Users.approveGuestCard(guestCard.id, guestCard.rowVersion, guestCardPriority)
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { viewState.showProgress(false) }
                .subscribe({
                    viewState.onApproveGuestCard(guestCard)
                }, {
                    viewState.showErrorMessage(R.string.error_general)
                })
    }

    fun sendForApprovalGuestCard(guestCard: GuestCard) {
        viewState.showProgress(true)

        Api.Users.sendForApprovalGuestCard(guestCard.id, guestCard.rowVersion)
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { viewState.showProgress(false) }
                .subscribe({
                    viewState.onChangeRequestGuestCard(guestCard)
                }, {
                    viewState.showErrorMessage(R.string.error_general)
                })
    }

    fun changeRequestGuestCard(guestCard: GuestCard, description: Description) {
        viewState.showProgress(true)

        Api.Users.changeRequestGuestCard(guestCard.id, guestCard.rowVersion, description)
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { viewState.showProgress(false) }
                .subscribe({
                    viewState.onChangeRequestGuestCard(guestCard)
                }, {
                    viewState.showErrorMessage(R.string.error_general)
                })
    }

    fun rejectGuestCard(guestCard: GuestCard, refused : Description) {
        viewState.showProgress(true)

        Api.Users.rejectGuestCard(guestCard.id, guestCard.rowVersion, refused)
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { viewState.showProgress(false) }
                .subscribe({
                    viewState.onRejectGuestCard(guestCard)
                }, {
                    viewState.showErrorMessage(R.string.error_general)
                })
    }

    fun deleteGuestCard(guestCard: GuestCard) {
        viewState.showProgress(true)

        Api.Users.deleteGuestCard(guestCard.id, guestCard.rowVersion)
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { viewState.showProgress(false) }
                .subscribe({
                    viewState.onDeleteGuestCard(guestCard)
                }, {
                    viewState.showErrorMessage(R.string.error_general)
                })
    }
}