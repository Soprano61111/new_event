package com.digitalhorizon.eve.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.api.Api
import com.digitalhorizon.eve.common.AccountManager
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.common.ScreenPool
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCardRequest
import com.digitalhorizon.eve.mvp.model.StafferCard
import com.digitalhorizon.eve.mvp.view.EditGuestCardView
import com.digitalhorizon.eve.ui.fragment.editguestcard.EditGuestCardFragment
import com.digitalhorizon.eve.utils.ImageUtils
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import java.io.File

@InjectViewState
class EditGuestCardPresenter : MvpPresenter<EditGuestCardView>() {
    val router = ApplicationWrapper.INSTANCE.getRouter()
    var photoFile: File? = null
    var card: GuestCard? = null
    var cardRequest: GuestCardRequest? = null

    fun getResponsible() {
        val stafferCard = AccountManager.userCard as? StafferCard
        stafferCard?.let { it ->
            Api.Users.getResponsible(it.structureUnitRoles[0].structureUnitId)
                    .subscribeOn(Schedulers.io())
                    .observeOn(AndroidSchedulers.mainThread())
                    .subscribe({ responsibleList ->
                        EditGuestCardFragment.responsibleList = responsibleList.items
                        viewState.setResponsible(responsibleList)
                    }, {
                        viewState.showToast(R.string.error_general)
                    })
        }
    }

    fun savePhoto() {
        viewState.showProgress(true)
        photoFile?.let { photoFile ->
            ImageUtils.send(photoFile)
                    .subscribeOn(AndroidSchedulers.mainThread())
                    .subscribe({ photo ->
                        viewState.showProgress(false)
                        cardRequest?.photo?.pictureId = photo.pictureId
                        card?.photo = photo
                        viewState.getCardFields()
                        saveCard()
                    }, {
                        viewState.showProgress(false)
                        viewState.showToast(R.string.error_general)
                    })
        }
    }

    fun saveCard() {
        viewState.showProgress(true)
        Api.Users.sendGuestCard(card?.id!!, cardRequest, card?.rowVersion)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    viewState.showProgress(false)
                    card = it
                    viewState.showToast(R.string.edit_guest_card_successfully_saved)
                    router.newRootScreen(ScreenPool.APPROVE_LIST_FRAGMENT)
                }, { e ->
                    viewState.showProgress(false)
                    viewState.showToast(R.string.error_general)
                })
    }
}