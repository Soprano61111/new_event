package com.digitalhorizon.eve.mvp.model.error

import com.google.gson.annotations.SerializedName

class Detail {
    @SerializedName("token")
    var validationToken: String? = null

    @SerializedName("errors")
    var errors: ArrayList<Errors>? = null

    @SerializedName("timeout")
    var timeout: Timeout? = null
}