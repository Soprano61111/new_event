package com.digitalhorizon.eve.mvp.model

import com.google.gson.annotations.SerializedName
import java.io.Serializable

class UpdateGuestPriority : Serializable {
    @SerializedName("guest_priority_id")
    var guestPriorityId: Int = 0
}