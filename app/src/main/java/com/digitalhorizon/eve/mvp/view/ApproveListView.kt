package com.digitalhorizon.eve.mvp.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.digitalhorizon.eve.mvp.model.ApproveList
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard
import com.digitalhorizon.eve.mvp.model.StafferCard

@StateStrategyType(SkipStrategy::class)
interface ApproveListView : MvpView {
    fun showToast(resId: Int)
    fun onReload()
    fun onLoadItems(items: ApproveList)
    fun checkGuestPriorities(card: GuestCard)
    fun showRejectDialog(guestCard: GuestCard)
    fun showApproveDialog(guestCard: GuestCard)
    fun onRejectGuestCard(guestCard: GuestCard)
    fun onApproveGuestCard(guestCard: GuestCard)
    fun setSUOCard(card: StafferCard)
}