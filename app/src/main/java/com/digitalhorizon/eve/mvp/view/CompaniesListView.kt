package com.digitalhorizon.eve.mvp.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.digitalhorizon.eve.mvp.model.CompaniesList

@StateStrategyType(SkipStrategy::class)
interface CompaniesListView : MvpView {
    fun onReload()
    fun onLoadItems(items: CompaniesList)
    fun showToast(resId: Int)
}