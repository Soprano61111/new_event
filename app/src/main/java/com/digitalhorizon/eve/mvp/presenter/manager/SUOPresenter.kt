package com.digitalhorizon.eve.mvp.presenter.manager

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.mvp.view.manager.SUOView

@InjectViewState
class SUOPresenter : MvpPresenter<SUOView>() {

}