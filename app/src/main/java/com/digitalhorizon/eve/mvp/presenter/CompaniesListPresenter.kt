package com.digitalhorizon.eve.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.api.Api
import com.digitalhorizon.eve.mvp.view.CompaniesListView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

@InjectViewState
class CompaniesListPresenter : MvpPresenter<CompaniesListView>() {
    val ITEMS_PRE_PAGE = 10
    var itemsPage = 0
    var lastLoadedCount = 0
    var companyName: String? = null
    var orderField: String? = null
    var sortOrder: String? = null

    fun loadCompanies() {
        Api.Users.getCompanies(companyName, ITEMS_PRE_PAGE, itemsPage * ITEMS_PRE_PAGE, orderField, sortOrder)
                .subscribeOn(Schedulers.io())
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({ companies ->
                    viewState.onLoadItems(companies)
                }, {
                    viewState.showToast(R.string.error_general)
                })
    }

    fun isLoadedAllItems(): Boolean {
        if (lastLoadedCount == ITEMS_PRE_PAGE || itemsPage == 0) {
            return false
        }
        return true
    }

    private fun resetList() {
        itemsPage = 0
        lastLoadedCount = 0
        viewState.onReload()
    }

    fun pullToRefresh() {
        resetList()
    }
}