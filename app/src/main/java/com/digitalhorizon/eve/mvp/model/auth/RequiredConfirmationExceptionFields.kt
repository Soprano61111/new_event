package com.digitalhorizon.eve.mvp.model.auth

import com.digitalhorizon.eve.mvp.model.error.Errors
import com.digitalhorizon.eve.mvp.model.error.Timeout
import com.google.gson.annotations.SerializedName
import org.json.JSONObject


class RequiredConfirmationExceptionFields(body: JSONObject) {

    @SerializedName("timeout")
    var timeout: Timeout? = null

    @SerializedName("errors")
    var errors: Errors? = null

    @SerializedName("token")
    var token: String? = null
}