package com.digitalhorizon.eve.mvp.view.guestcard

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType

@StateStrategyType(SkipStrategy::class)
interface GuestCardForSecurityView : BaseGuestCardView {
    fun setDenyCount(count: Int)
}