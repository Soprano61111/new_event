package com.digitalhorizon.eve.mvp.presenter

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.R
import com.digitalhorizon.eve.api.Api
import com.digitalhorizon.eve.api.HttpStatusCode
import com.digitalhorizon.eve.common.AccountManager
import com.digitalhorizon.eve.common.ApplicationWrapper
import com.digitalhorizon.eve.common.ScreenPool
import com.digitalhorizon.eve.common.Settings
import com.digitalhorizon.eve.mvp.model.Phone
import com.digitalhorizon.eve.mvp.model.auth.Registration
import com.digitalhorizon.eve.mvp.model.auth.SmsRegistration
import com.digitalhorizon.eve.mvp.model.device.DeviceInfo
import com.digitalhorizon.eve.mvp.model.error.EnumErrorType
import com.digitalhorizon.eve.mvp.model.error.EnumReasons
import com.digitalhorizon.eve.mvp.view.RegistrationView
import com.digitalhorizon.eve.utils.ErrorUtils
import com.digitalhorizon.eve.utils.ServerException
import com.digitalhorizon.eve.utils.TimerUtils
import com.pawegio.kandroid.d
import io.reactivex.android.schedulers.AndroidSchedulers

@InjectViewState
class RegistrationPresenter : MvpPresenter<RegistrationView>() {
    lateinit var deviceInfo: DeviceInfo
    val router = ApplicationWrapper.INSTANCE.getRouter()

    fun onPhoneConfirm(phone: String) {
        val registration = Registration(phone)
        registration.device = deviceInfo
        registration(registration)
    }

    private fun registration(registration: Registration, isRetryIn: Boolean = false) {
        viewState.showProgress(true)

        Api.Auth.register(registration)
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { viewState.showProgress(false) }
                .subscribe({
                    if (!isRetryIn) {
                        router.navigateTo(ScreenPool.SMS_CONFIRMATION_FRAGMENT, registration)
                    }
                }, { e ->
                    val error = ErrorUtils.parseError(e)
                    parseError(error, registration)
                })
    }

    fun sendAgainSmsCode(registration: Registration) {
        Settings.ValidationToken = null
        registration(registration, true)
    }

    fun checkPhoneNumber(phoneNumber: String) {
        viewState.showCheckPhoneProgress(true)
        //TODO: refactoring, set completable
        Api.Auth.checkPhone(Phone(phoneNumber))
                .observeOn(AndroidSchedulers.mainThread())
                .subscribe({
                    when (it.code()) {
                        HttpStatusCode.NO_CONTENT,
                        HttpStatusCode.OK -> {
                            viewState.onCheckedPhone(true)
                            viewState.showCheckPhoneProgress(false)
                        }
                        HttpStatusCode.NOT_FOUND -> {
                            viewState.onCheckedPhone(false)
                            viewState.showCheckPhoneProgress(false)
                        }
                    }
                }, { e ->
                    viewState.showErrorMessage(true)
                })
    }

    fun confirmSmsCode(registration: Registration, smsCode: String) {
        viewState.showProgress(true)

        Api.Auth.register(registration, smsCode)
                .observeOn(AndroidSchedulers.mainThread())
                .doFinally { viewState.showProgress(false) }
                .subscribe({ result ->
                    Settings.AccessToken = result.accessToken
                    Settings.ValidationToken = null
                    AccountManager.role = result.roles.first()
                    router.newRootScreen(ScreenPool.MAIN_FRAGMENT)
                }, { e ->
                    val error = ErrorUtils.parseError(e)
                    parseError(error, registration)
                })
    }

    private fun parseError(error: ServerException, registration: Registration) {
        when(error.type) {
            EnumErrorType.REQUIRED_CONFIRMATION -> {
                val timeout = TimerUtils().timeToMillis(error.extraFields?.timeout?.elapsed)
                Settings.ValidationToken = error.extraFields?.validationToken
                router.navigateTo(ScreenPool.SMS_CONFIRMATION_FRAGMENT, SmsRegistration(registration, timeout))
            }
            EnumErrorType.MODEL_VALIDATION -> {
                d("ERROR_TYPE", "validation model")
            }
            EnumErrorType.TIMEOUT -> {
                val timeout = TimerUtils().timeToMillis(error.extraFields?.timeout?.elapsed)
                viewState.showErrorMessage(true, R.string.auth_user_blocked, timeout, true)
            }
            EnumErrorType.ERRORS -> {
                val reasons = error.extraFields?.errors?.get(0)?.reasons?.get(0)

                when(reasons) {
                    EnumReasons.WRONG_SMS_CODE -> {
                        viewState.showErrorMessage(true, R.string.auth_wrong_sms_code)
                    }

                    else -> {
                        viewState.showErrorMessage(true, R.string.error_general)
                    }
                }
            }
        }
    }
}