package com.digitalhorizon.eve.mvp.view.guestcard

import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard

@StateStrategyType(SkipStrategy::class)
interface GuestCardForSUOView : BaseGuestCardView {
    fun onRejectGuestCard(guestCard: GuestCard)
    fun onApproveGuestCard(guestCard: GuestCard)
    fun onDeleteGuestCard(guestCard: GuestCard)
    fun onSendForApprovalGuestCard(guestCard: GuestCard)
    fun onChangeRequestGuestCard(guestCard: GuestCard)
    fun showProgress(isVisible: Boolean)
}