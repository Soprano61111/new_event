package com.digitalhorizon.eve.mvp.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.digitalhorizon.eve.mvp.model.StructureUnitList

@StateStrategyType(SkipStrategy::class)
interface StructureUnitListView : MvpView {
    fun onLoadItems(items: StructureUnitList)
    fun onReload()
    fun showToast(resId: Int)
}