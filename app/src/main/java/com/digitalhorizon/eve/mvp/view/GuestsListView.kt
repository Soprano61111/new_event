package com.digitalhorizon.eve.mvp.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.digitalhorizon.eve.mvp.model.CardsList
import com.digitalhorizon.eve.mvp.model.guestcard.GuestCard

@StateStrategyType(SkipStrategy::class)
interface GuestsListView : MvpView {
    fun showRejectDialog(guestCard: GuestCard)
    fun showApproveDialog(guestCard: GuestCard)
    fun onRejectGuestCard(guestCard: GuestCard)
    fun onApproveGuestCard(guestCard: GuestCard)
    fun showToast(resId: Int)
    fun onReload()
    fun onLoadItems(items: CardsList)
}