package com.digitalhorizon.eve.mvp.model.auth

import java.io.Serializable

class SmsRegistration(registration: Registration, countdown: Long) : Serializable {
    var regData: Registration = registration
    var countdownTimeout: Long = countdown
}