package com.digitalhorizon.eve.mvp.presenter.manager

import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpPresenter
import com.digitalhorizon.eve.mvp.view.manager.ManagerView

@InjectViewState
class ManagerPresenter : MvpPresenter<ManagerView>() {
    val ITEMS_PRE_PAGE = 10
    var itemsPage = 0
    var lastLoadedCount = 0
    var query = ""

    fun getGuestCards() {

    }

    fun isLoadedAllItems(): Boolean {
        if (lastLoadedCount == ITEMS_PRE_PAGE || itemsPage == 0) {
            return false
        }
        return true
    }

    fun refreshFoundList() {
        itemsPage = 0
        lastLoadedCount = 0
        viewState.onRefreshList()
    }
}