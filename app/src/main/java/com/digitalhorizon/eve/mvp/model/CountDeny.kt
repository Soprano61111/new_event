package com.digitalhorizon.eve.mvp.model
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class CountDeny : Serializable {
    @SerializedName("count")
    val denyCount: Int = 0
}
