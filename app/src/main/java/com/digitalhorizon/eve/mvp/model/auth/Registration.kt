package com.digitalhorizon.eve.mvp.model.auth

import com.digitalhorizon.eve.mvp.model.device.DeviceInfo
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class Registration(login: String) : Serializable {
    @SerializedName("login")
    var login: String = login

    @SerializedName("device")
    var device: DeviceInfo = DeviceInfo()
}