package com.digitalhorizon.eve.mvp.model.auth

import com.digitalhorizon.eve.mvp.model.RoleEnum
import com.google.gson.annotations.SerializedName
import java.io.Serializable

class AuthResult : Serializable {
    @SerializedName("access_token")
    val accessToken: String? = null

    @SerializedName("permissions")
    val permissions: Array<String> = emptyArray()

    @SerializedName("roles")
    val roles: ArrayList<RoleEnum> = ArrayList()

    @SerializedName("server_signature")
    val serverSignature: String? = null
}