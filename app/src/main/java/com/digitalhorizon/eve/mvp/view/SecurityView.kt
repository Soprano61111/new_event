package com.digitalhorizon.eve.mvp.view

import com.arellomobile.mvp.MvpView
import com.arellomobile.mvp.viewstate.strategy.SkipStrategy
import com.arellomobile.mvp.viewstate.strategy.StateStrategyType
import com.digitalhorizon.eve.mvp.model.StafferCard

@StateStrategyType(SkipStrategy::class)
interface SecurityView : MvpView {
    fun showViewGuestNotFound()
    fun progressBar(isVisible: Boolean)
    fun setSecurityCard(card: StafferCard)
}
